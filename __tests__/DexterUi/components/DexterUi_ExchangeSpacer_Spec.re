open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode; No onClick", () => {
    render(<Helpers_Context> <DexterUi_ExchangeSpacer /> </Helpers_Context>)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; No onClick", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_ExchangeSpacer />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; With onClick", () => {
    render(
      <Helpers_Context>
        <DexterUi_ExchangeSpacer onClick={_ => ()} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; With onClick", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_ExchangeSpacer onClick={_ => ()} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
