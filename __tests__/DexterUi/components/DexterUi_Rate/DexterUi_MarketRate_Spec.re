open Jest;
open Expect;
open ReactTestingLibrary;

let display = DexterUi_MarketRate.display;

describe("display", () => {
  test("tzgExchangeBalance", () => {
    expect(
      display(
        Fixtures_Balances.tzgExchangeBalance.exchangeXtz,
        Fixtures_Balances.tzgExchangeBalance.exchangeTokenBalance,
        Fixtures_Balances.tzgExchangeBalance.symbol,
      ),
    )
    |> toEqual("10000 TZG = 1 XTZ")
  });

  test("mtzExchangeBalance", () => {
    expect(
      display(
        Fixtures_Balances.mtzExchangeBalance.exchangeXtz,
        Fixtures_Balances.mtzExchangeBalance.exchangeTokenBalance,
        Fixtures_Balances.mtzExchangeBalance.symbol,
      ),
    )
    |> toEqual("0.015238 MTZ = 1 XTZ")
  });

  test("tztExchangeBalance", () => {
    expect(
      display(
        Fixtures_Balances.tztExchangeBalance.exchangeXtz,
        Fixtures_Balances.tztExchangeBalance.exchangeTokenBalance,
        Fixtures_Balances.tztExchangeBalance.symbol,
      ),
    )
    |> toEqual("98.910891 TZT = 1 XTZ")
  });
});

describe("Snapshots", () => {
  test("tzgExchangeBalance", () => {
    render(
      <DexterUi_MarketRate
        xtzPool={Fixtures_Balances.tzgExchangeBalance.exchangeXtz}
        tokenPool={Fixtures_Balances.tzgExchangeBalance.exchangeTokenBalance}
        symbol={Fixtures_Balances.tzgExchangeBalance.symbol}
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });

  test("mtzExchangeBalance", () => {
    render(
      <DexterUi_MarketRate
        xtzPool={Fixtures_Balances.mtzExchangeBalance.exchangeXtz}
        tokenPool={Fixtures_Balances.mtzExchangeBalance.exchangeTokenBalance}
        symbol={Fixtures_Balances.mtzExchangeBalance.symbol}
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });

  test("tztExchangeBalance", () => {
    render(
      <DexterUi_MarketRate
        xtzPool={Fixtures_Balances.tztExchangeBalance.exchangeXtz}
        tokenPool={Fixtures_Balances.tztExchangeBalance.exchangeTokenBalance}
        symbol={Fixtures_Balances.tztExchangeBalance.symbol}
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
