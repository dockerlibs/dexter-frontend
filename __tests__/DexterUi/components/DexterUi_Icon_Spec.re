open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode", () => {
    render(
      <Helpers_Context> <DexterUi_Icon name="exchange" /> </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; No darkModeSuffix", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Icon size=15 name="exchange" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; With darkModeSuffix", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Icon size=15 name="exchange" darkModeSuffix="-d" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
