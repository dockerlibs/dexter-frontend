open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Selected", () => {
    render(<DexterUi_Radio isSelected=true />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Not selected; Size 14px", () => {
    render(<DexterUi_Radio isSelected=false size=14 />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
