open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(
      <DexterUi_EmptyMessage>
        {"Snapshot" |> React.string}
      </DexterUi_EmptyMessage>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
