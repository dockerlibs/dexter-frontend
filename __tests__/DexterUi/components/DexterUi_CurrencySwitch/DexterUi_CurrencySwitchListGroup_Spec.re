open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currentCurrency USD; popular; empty value", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitchListGroup popular=true value="" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency BTC; popular; value=BTC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitchListGroup popular=true value="BTC" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency USD; popular; value=ABC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitchListGroup popular=true value="ABC" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency USD; not popular; value empty", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitchListGroup value="" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency BTC; not popular; value=Sri", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitchListGroup value="Sri" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency BTC; not popular; value=ABC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitchListGroup value="ABC" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
