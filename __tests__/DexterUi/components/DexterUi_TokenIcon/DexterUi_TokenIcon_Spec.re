open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Normal tzBTC", () => {
    render(<DexterUi_TokenIcon icon="/img/tzBTC-logo.png" />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Small USDtz", () => {
    render(<DexterUi_TokenIcon icon="/img/USDtz-logo.png" small=true />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
