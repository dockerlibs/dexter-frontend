open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No account", () => {
    render(<DexterUi_Transactions />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Transactions tab", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1 transactions=[]>
        <DexterUi_Transactions />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("PoolTokens tab", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1>
        <DexterUi_Transactions defaultTab=PoolTokens />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
