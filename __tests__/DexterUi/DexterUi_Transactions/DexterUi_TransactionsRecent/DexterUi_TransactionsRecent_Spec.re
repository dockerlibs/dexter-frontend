open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No transactions fetched", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1>
        <DexterUi_TransactionsRecent />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("No transactions", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1 transactions=[]>
        <DexterUi_TransactionsRecent />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With transactions", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchange]>
        <DexterUi_TransactionsRecent />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZKT unavailable", () => {
    render(
      <Helpers_Context unavailableServices=[|TZKT|]>
        <DexterUi_TransactionsRecent />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
