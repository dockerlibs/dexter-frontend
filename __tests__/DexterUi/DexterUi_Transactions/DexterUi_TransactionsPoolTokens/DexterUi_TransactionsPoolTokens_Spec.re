open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No pool tokens", () => {
    render(<DexterUi_TransactionsPoolTokens />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With pool token, but no lqtBalance", () => {
    render(
      <Helpers_Context balances=[Fixtures_Balances.tztBalance]>
        <DexterUi_TransactionsPoolTokens />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With pool token", () => {
    render(
      <Helpers_Context balances=[Fixtures_Balances.mtzBalance]>
        <DexterUi_TransactionsPoolTokens />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
