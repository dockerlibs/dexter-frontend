open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Non-empty balances", () => {
    render(
      <Helpers_Context balances=Fixtures_Balances.balances1>
        <DexterUi_WalletInfoBalances />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Empty balances", () => {
    render(
      <Helpers_Context> <DexterUi_WalletInfoBalances /> </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
