open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Render", () => {
    render(
      <DexterUi_WalletInfo
        account={
          ...Fixtures_Account.account1,
          headBlock: {
            ...Fixtures_Account.account1.headBlock,
            header: {
              ...Fixtures_Account.account1.headBlock.header,
              timestamp:
                Tezos_Timestamp.Timestamp(
                  MomentRe.moment("2020-09-30T00:00:01.000Z"),
                ),
            },
          },
        }
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
