open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi_PoolDataHeader />)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
