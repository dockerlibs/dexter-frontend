open Jest;
open Expect;

describe("truncateDecimals", () => {
  test("3, 0., 0", () => {
    expect(0. |> Format.truncateDecimals(3)) |> toBe("0")
  });
  test("3, 0.012345, 0.012", () => {
    expect(0.012345 |> Format.truncateDecimals(3)) |> toBe("0.012")
  });
  test("3, 0.098765, 0.099", () => {
    expect(0.098765 |> Format.truncateDecimals(3)) |> toBe("0.099")
  });
  test("3, 0.001234, 0.001", () => {
    expect(0.001234 |> Format.truncateDecimals(3)) |> toBe("0.001")
  });
  test("3, 0.0009, <0.001", () => {
    expect(0.0009 |> Format.truncateDecimals(3)) |> toBe("<0.001")
  });
});
