open Jest;
open Expect;

describe("omitIpfsInPath", () => {
  test("['ipns', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7'], []", () =>
    expect(
      Utils.omitIpfsInPath([
        "ipns",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
      ]),
    )
    |> toEqual([])
  );
  test(
    "['ipfs', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7', 'liquidity', 'add'], ['liquidity', 'add']",
    () =>
    expect(
      Utils.omitIpfsInPath([
        "ipfs",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
        "liquidity",
        "add",
      ]),
    )
    |> toEqual(["liquidity", "add"])
  );
  test("['liquidity', 'add'], ['liquidity', 'add']", () =>
    expect(Utils.omitIpfsInPath(["liquidity", "add"]))
    |> toEqual(["liquidity", "add"])
  );
});

describe("getIpfsBaseUrlFromPath", () => {
  test("['ipns', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7'], []", () =>
    expect(
      Utils.getIpfsBaseUrlFromPath([
        "ipns",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
      ]),
    )
    |> toEqual("/ipns/QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7/")
  );
  test(
    "['ipfs', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7', 'liquidity', 'add'], ['liquidity', 'add']",
    () =>
    expect(
      Utils.getIpfsBaseUrlFromPath([
        "ipfs",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
        "liquidity",
        "add",
      ]),
    )
    |> toEqual("/ipfs/QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7/")
  );
  test("['liquidity', 'add'], ['liquidity', 'add']", () =>
    expect(Utils.getIpfsBaseUrlFromPath(["liquidity", "add"]))
    |> toEqual("/")
  );
});
