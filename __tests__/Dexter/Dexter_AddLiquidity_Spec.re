open Jest;
open Expect;
open Dexter_AddLiquidity;

exception TestError(string);

let ofStringUnsafe = s => {
  switch (Tezos_Mutez.ofString(s)) {
  | Belt.Result.Ok(s) => s
  | Belt.Result.Error(err) => raise @@ TestError(err)
  };
};

describe("addLiquidityTokenIn", () => {
  test("", () => {
    expect(
      addLiquidityTokenIn(
        Tezos_Mutez.Mutez(10000000L),
        Tezos_Mutez.Mutez(6000000000L),
        Tezos_Token.mkToken(Bigint.of_int64(100000000L), 0),
      ),
    )
    |> toEqual(Some(Tezos_Token.mkToken(Bigint.of_int64(166667L), 0)))
  })
});

describe("addLiquidityXtzIn", () => {
  test("", ()
    => {
      expect(
        addLiquidityXtzIn(
          Tezos_Token.mkToken(Bigint.of_int64(1500000L), 0),
          Tezos_Mutez.Mutez(1000000L),
          Tezos_Token.mkToken(Bigint.of_int64(500000L), 0),
        ),
      )
      |> toEqual(Some(Tezos_Mutez.Mutez(3000000L)))
    })
});

describe("removeLiquidityXtzTokenOut", () => {
  test("", ()
    => {
      expect(
        removeLiquidityXtzTokenOut(
          ~liquidityBurned=Tezos_Token.mkToken(Bigint.of_int64(5000000L), 0),
          ~totalLiquidity=Tezos_Token.mkToken(Bigint.of_int64(100000000L), 0),
          ~tokenPool=Tezos_Token.mkToken(Bigint.of_int64(5000000L), 0),
          ~xtzPool=Tezos_Mutez.Mutez(5000000L),
          (),
        ),
      )
      |> toEqual((Tezos_Mutez.Mutez(250000L), Tezos_Token.mkToken(Bigint.of_int64(250000L), 0)))
    });

  test("", ()
    => {
      expect(
        removeLiquidityXtzTokenOut(
          ~liquidityBurned=Tezos_Token.mkToken(Bigint.of_int64(2600000L), 0),
          ~totalLiquidity=Tezos_Token.mkToken(Bigint.of_int64(100000000L), 0),
          ~tokenPool=Tezos_Token.mkToken(Bigint.of_int64(70000000L), 0),
          ~xtzPool=Tezos_Mutez.Mutez(6600000L),
          (),
        ),
      )
      |> toEqual((Tezos_Mutez.Mutez(171600L), Tezos_Token.mkToken(Bigint.of_int64(1820000L), 0)))
    })

  test("half", ()
    => {
      expect(
        removeLiquidityXtzTokenOut(
          ~liquidityBurned=Tezos_Token.mkToken(Bigint.of_string("5000000000"), 0),
          ~totalLiquidity=Tezos_Token.mkToken(Bigint.of_string("10000000000"), 0),
          ~tokenPool=Tezos_Token.mkToken(Bigint.of_string("3000000000"), 6),
          ~xtzPool=ofStringUnsafe("1000000000"),
          (),
        ),
      )
      |> toEqual((ofStringUnsafe("500000000"), Tezos_Token.mkToken(Bigint.of_string("1500000000"), 6)))
    })      

  test("fourth", ()
    => {
      expect(
        removeLiquidityXtzTokenOut(
          ~liquidityBurned=Tezos_Token.mkToken(Bigint.of_string("2500000000"), 0),
          ~totalLiquidity=Tezos_Token.mkToken(Bigint.of_string("10000000000"), 0),
          ~tokenPool=Tezos_Token.mkToken(Bigint.of_string("1000000000"), 8),
          ~xtzPool=ofStringUnsafe("200000000000"),
          (),
        ),
      )
      |> toEqual((ofStringUnsafe("50000000000"), Tezos_Token.mkToken(Bigint.of_string("250000000"), 8)))
    })      
});
