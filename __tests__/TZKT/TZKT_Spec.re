open Jest;
open Expect;

let fp = "__tests__/golden/";

describe("decode", () => {
  test("decode TZKT_payload_1.json", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_payload_1.json"),
      );
    expect(Belt.Result.isOk(TZKT.Operation.decode(json))) |> toEqual(true);
  });

  test("decode TZKT_payload_2.json", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_payload_2.json"),
      );
    expect(Belt.Result.isOk(TZKT.Operation.decode(json))) |> toEqual(true);
  });

  test("decode TZKT_payload_3.json", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_payload_3.json"),
      );
    expect(Belt.Result.isOk(TZKT.Operation.decode(json))) |> toEqual(true);
  });
});

let account =
  Tezos_Address.ofString("tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM")
  |> Common.unwrapResult;

let exchanges: list(Dexter_Exchange.t) = [
  {
    name: "tzBTC",
    symbol: "tzBTC",
    decimals: 8,
    iconDark: "tzBTC-logo.png",    
    iconLight: "tzBTC-logo.png",
    tokenContract:
      Tezos.Contract.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
      |> Common.unwrapResult,
    dexterContract:
      Tezos.Contract.ofString("KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf")
      |> Common.unwrapResult,
    dexterBigMapId: Tezos.BigMapId.ofInt(123),
  },
  {
    name: "USD Tez",
    symbol: "USDtz",
    decimals: 6,
    iconDark: "USDtz-logo.png",
    iconLight: "USDtz-logo.png",
    tokenContract:
      Tezos.Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
      |> Common.unwrapResult,
    dexterContract:
      Tezos.Contract.ofString("KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD")
      |> Common.unwrapResult,
    dexterBigMapId: Tezos.BigMapId.ofInt(124),
  },
];

describe("xtzToToken", () => {
  test("exchange xtzToToken", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_xtzToToken.json"),
      );
    let transactions =
      Json.Decode.list(
        a => Tezos_Util.unwrapResult(TZKT.Operation.decode(a)),
        json,
      );
    expect(Dexter_Transaction.ofOperations(account, exchanges, transactions))
    |> toEqual(
         Some(
           {
             block: "BKvbxS9NHZ1znW5nEzhi79zV6ddTdouU11y3fCxi5D8W1BNs3ZP",
             contractId: "",
             hash: "opJeQutU23hjBdZhDnhEKw45vTYB787nE6CDMWAqMQ8CtBRWyyt",
             timestamp: Timestamp(MomentRe.moment("2020-10-11T01:00:44Z")),
             status: TZKT.Status.Applied,
             walletAddress: account,
             transactionType:
               Exchange({
                 valueIn:
                   InputType.Mutez(
                     Tezos_Mutez.ofInt64(1000000000L) |> Common.unwrapResult,
                   ),
                 symbolIn: "XTZ",
                 valueOut:
                   InputType.Token(
                     Tezos_Token.ofInt64(19089730L, 8) |> Common.unwrapResult,
                   ),
                 symbolOut: "tzBTC",
                 destination:
                   Tezos.Address.ofString(
                     "tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM",
                   )
                   |> Common.unwrapResult,
               }),
           }: Dexter_Transaction.t,
         ),
       );
  })
});

describe("tokenToXtz", () => {
  test("exchange tokenToXtz", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_tokenToXtz.json"),
      );
    let transactions =
      Json.Decode.list(
        a => Tezos_Util.unwrapResult(TZKT.Operation.decode(a)),
        json,
      );
    expect(Dexter_Transaction.ofOperations(account, exchanges, transactions))
    |> toEqual(
         Some(
           {
             block: "BLZYmTkezVieNS5CSgiiZGYheqbCZ1ab2oA9MFPeadDV6VB3C5b",
             contractId: "",
             hash: "ooSiihVB3jYX6s91pSZ3Dk5Wuznj3M3UyVDxoesgyLo5waUeZwH",
             timestamp: Timestamp(MomentRe.moment("2020-10-11T00:06:44Z")),
             status: TZKT.Status.Applied,
             walletAddress: account,
             transactionType:
               Exchange({
                 valueIn:
                   InputType.Token(
                     Tezos_Token.ofInt64(726896912L, 6) |> Common.unwrapResult,
                   ),
                 valueOut:
                   InputType.Mutez(
                     Tezos_Mutez.ofInt64(291683372L) |> Common.unwrapResult,
                   ),
                 symbolIn: "USDtz",
                 symbolOut: "XTZ",
                 destination:
                   Tezos.Address.ofString(
                     "tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM",
                   )
                   |> Common.unwrapResult,
               }),
           }: Dexter_Transaction.t,
         ),
       );
  })
});

describe("addLiquidity", () => {
  test("addLiquidity", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_addLiquidity.json"),
      );
    let transactions =
      Json.Decode.list(
        a => Tezos_Util.unwrapResult(TZKT.Operation.decode(a)),
        json,
      );
    expect(Dexter_Transaction.ofOperations(account, exchanges, transactions))
    |> toEqual(
         Some(
           {
             block: "BLdN6v8DBmsJXrHXUmC2zz7zPK5x8iYzsYLkx66ofgg8qhotGdn",
             contractId: "",
             hash: "oo8huv677AxRTa7tj4ZKRSrtrn66SmN3QrAk3xmG7cqTPh1VacP",
             timestamp: Timestamp(MomentRe.moment("2020-10-11T01:02:44Z")),
             status: TZKT.Status.Applied,
             walletAddress: account,
             transactionType:
               AddLiquidity({
                 xtzIn:
                   Tezos_Mutez.ofInt64(1014983249L) |> Common.unwrapResult,
                 tokenIn:
                   Tezos_Token.ofInt64(19089730L, 8) |> Common.unwrapResult,
                 symbol: "tzBTC",
               }),
           }: Dexter_Transaction.t,
         ),
       );
  })
});

describe("removeLiquidity", () => {
  test("removeLiquidity", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_removeLiquidity.json"),
      );
    let transactions =
      Json.Decode.list(
        a => Tezos_Util.unwrapResult(TZKT.Operation.decode(a)),
        json,
      );
    expect(Dexter_Transaction.ofOperations(account, exchanges, transactions))
    |> toEqual(
         Some(
           {
             block: "BL2RtSw3bHYKnQ33YhXLKFeziVjz881cdzNTtCTzPra7cDCmcU1",
             contractId: "",
             hash: "opJbMK4TbPo3g3rUHAk3aYZ9xynjGmZVMW5wZ8bSyahJT1xosgu",
             timestamp: Timestamp(MomentRe.moment("2020-10-10T23:58:44Z")),
             status: TZKT.Status.Applied,
             walletAddress: account,
             transactionType:
               RemoveLiquidity({
                 lqtBurned:
                   Tezos_Token.ofInt64(892569053L, 0) |> Common.unwrapResult,
                 xtzOut:
                   Tezos_Mutez.ofInt64(912141697L) |> Common.unwrapResult,
                 tokenOut:
                   Tezos_Token.ofInt64(1956058939L, 6) |> Common.unwrapResult,
                 symbol: "USDtz",
               }),
           }: Dexter_Transaction.t,
         ),
       );
  })
});

describe("multiple values", () => {
  test("removeLiquidity", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "TZKT_operations.json"),
      );
    let result =
      Json.Decode.list(
        a => Tezos_Util.unwrapResult(TZKT.Operation.decode(a)),
        json,
      )
      |> Dexter_Transaction.ofQuery(account, exchanges);

    expect(List.nth(result, 0))
    |> toEqual(
         {
           block: "BLdN6v8DBmsJXrHXUmC2zz7zPK5x8iYzsYLkx66ofgg8qhotGdn",
           hash: "oo8huv677AxRTa7tj4ZKRSrtrn66SmN3QrAk3xmG7cqTPh1VacP",
           timestamp: Timestamp(MomentRe.moment("2020-10-11T01:02:44Z")),
           contractId: "",
           walletAddress: account,
           status: TZKT.Status.Applied,
           transactionType:
             AddLiquidity({
               xtzIn: Tezos_Mutez.ofInt64(1014983249L) |> Common.unwrapResult,
               tokenIn:
                 Tezos_Token.ofInt64(19089730L, 8) |> Common.unwrapResult,
               symbol: "tzBTC",
             }),
         }: Dexter_Transaction.t,
       )
    |> ignore;

    expect(List.nth(result, 1))
    |> toEqual(
         {
           block: "BLZYmTkezVieNS5CSgiiZGYheqbCZ1ab2oA9MFPeadDV6VB3C5b",
           hash: "ooSiihVB3jYX6s91pSZ3Dk5Wuznj3M3UyVDxoesgyLo5waUeZwH",
           timestamp: Timestamp(MomentRe.moment("2020-10-11T00:06:44Z")),
           status: TZKT.Status.Applied,
           contractId: "",
           walletAddress: account,
           transactionType:
             Exchange({
               valueIn:
                 InputType.Token(
                   Tezos_Token.ofInt64(726896912L, 6) |> Common.unwrapResult,
                 ),
               valueOut:
                 InputType.Mutez(
                   Tezos_Mutez.ofInt64(291683372L) |> Common.unwrapResult,
                 ),
               symbolIn: "USDtz",
               symbolOut: "XTZ",
               destination:
                 Tezos.Address.ofString(
                   "tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM",
                 )
                 |> Common.unwrapResult,
             }),
         }: Dexter_Transaction.t,
       )
    |> ignore;

    expect(List.nth(result, 2))
    |> toEqual(
         {
           block: "BL2RtSw3bHYKnQ33YhXLKFeziVjz881cdzNTtCTzPra7cDCmcU1",
           hash: "opJbMK4TbPo3g3rUHAk3aYZ9xynjGmZVMW5wZ8bSyahJT1xosgu",
           timestamp: Timestamp(MomentRe.moment("2020-10-10T23:58:44Z")),
           contractId: "",
           walletAddress: account,
           status: TZKT.Status.Applied,
           transactionType:
             RemoveLiquidity({
               lqtBurned:
                 Tezos_Token.ofInt64(892569053L, 0) |> Common.unwrapResult,
               xtzOut: Tezos_Mutez.ofInt64(912141697L) |> Common.unwrapResult,
               tokenOut:
                 Tezos_Token.ofInt64(1956058939L, 6) |> Common.unwrapResult,
               symbol: "USDtz",
             }),
         }: Dexter_Transaction.t,
       );
  })
});
