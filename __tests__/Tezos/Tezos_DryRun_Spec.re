open Jest;
open Expect;

let expressions = Tezos_Expression.expressions;
let singleExpression = Tezos_Expression.singleExpression;

let readJson = filePath => {
  let json = Js.Json.parseExn(Node.Fs.readFileAsUtf8Sync(filePath));
  json;
};

let fp = "__tests__/golden/";

describe("building code expressions to be used in view contract", () => {
  test("failed transaction result", () => {
    expect(
      Tezos_DryRun.decodeFailedTransactionResult(
        readJson(fp ++ "failed_transaction_result.json"),
      ),
    )
    |> toEqual(
         Belt.Result.Ok(
           {
             errors: [
               {
                 kind: "temporary",
                 id: "proto.006-PsCARTHA.michelson_v1.script_rejected",
                 location: 7,
                 with_:
                   Tezos_Primitives.(
                     singleExpression(
                       ~prim=data(Tezos_PrimitiveData.Pair),
                       ~args=[
                         IntExpression(Bigint.of_string("1546544")),
                         singleExpression(
                           ~prim=data(Tezos_PrimitiveData.Unit),
                           (),
                         ),
                       ],
                       (),
                     )
                   ),
               },
             ],
           }: Tezos_DryRun.failedTransactionResult,
         ),
       )
  });

  test("internal operation result", () => {
    expect(
      Tezos_DryRun.decodeInternalOperationResult(
        readJson(fp ++ "internal_operation_result.json"),
      ),
    )
    |> toEqual(
         Belt.Result.Ok(
           {
             result: {
               errors: [
                 {
                   kind: "temporary",
                   id: "proto.006-PsCARTHA.michelson_v1.script_rejected",
                   location: 7,
                   with_:
                     Tezos_Primitives.(
                       singleExpression(
                         ~prim=data(Tezos_PrimitiveData.Pair),
                         ~args=[
                           IntExpression(Bigint.of_string("1546544")),
                           singleExpression(
                             ~prim=data(Tezos_PrimitiveData.Unit),
                             (),
                           ),
                         ],
                         (),
                       )
                     ),
                 },
               ],
             },
           }: Tezos_DryRun.internalOperationResult,
         ),
       )
  });

  test("metadata", () => {
    expect(Tezos_DryRun.decodeMetadata(readJson(fp ++ "metadata.json")))
    |> toEqual(
         Belt.Result.Ok(
           {
             internalOperationResults: [
               {
                 result: {
                   errors: [
                     {
                       kind: "temporary",
                       id: "proto.006-PsCARTHA.michelson_v1.script_rejected",
                       location: 7,
                       with_:
                         Tezos_Primitives.(
                           singleExpression(
                             ~prim=data(Tezos_PrimitiveData.Pair),
                             ~args=[
                               IntExpression(Bigint.of_string("1546544")),
                               singleExpression(
                                 ~prim=data(Tezos_PrimitiveData.Unit),
                                 (),
                               ),
                             ],
                             (),
                           )
                         ),
                     },
                   ],
                 },
               },
             ],
           }: Tezos_DryRun.metadata,
         ),
       )
  });

  test("dryRunResponse", () => {
    expect(
      Tezos_DryRun.decodeDryRunResponse(
        readJson(fp ++ "view_response.json"),
      ),
    )
    |> toEqual(
         Belt.Result.Ok(
           {
             contents: [
               {
                 internalOperationResults: [
                   {
                     result: {
                       errors: [
                         {
                           kind: "temporary",
                           id: "proto.006-PsCARTHA.michelson_v1.script_rejected",
                           location: 7,
                           with_:
                             Tezos_Primitives.(
                               singleExpression(
                                 ~prim=data(Tezos_PrimitiveData.Pair),
                                 ~args=[
                                   IntExpression(
                                     Bigint.of_string("1546544"),
                                   ),
                                   singleExpression(
                                     ~prim=data(Tezos_PrimitiveData.Unit),
                                     (),
                                   ),
                                 ],
                                 (),
                               )
                             ),
                         },
                       ],
                     },
                   },
                 ],
               },
             ],
           }: Tezos_DryRun.dryRunResponse,
         ),
       )
  });
});
