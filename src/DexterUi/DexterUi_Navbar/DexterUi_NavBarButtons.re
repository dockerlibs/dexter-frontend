type link = {
  href: string,
  text: string,
};

let links: array(link) = [|
  {href: "https://stats.dexter.exchange", text: "Dexter Stats"},
  {href: "https://dexter.exchange/docs/dexter-tutorials/", text: "Tutorials"},
  {
    href: "https://docs.google.com/forms/d/e/1FAIpQLSeM5hB6A4swBrpwMceGQZ3ioS_k3_SbqLESaea5svGwobUmeQ/viewform?usp=sf_link",
    text: "Share Feedback",
  },
|];

[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();

  <Flex
    flexDirection={isSm ? `column : `row}
    alignItems=`center
    justifyContent={isSm ? `center : `flexEnd}
    mt={`px(isSm ? 4 : 0)}>
    <Flex
      justifyContent=`flexEnd
      alignItems=`center
      flexDirection={isSm ? `column : `row}>
      {links
       |> Array.mapi((i, {href, text}) =>
            <Flex
              key={i |> string_of_int}
              alignItems=`center
              ml={isSm ? `zero : `px(20)}
              mt={isSm ? `px(8) : `zero}>
              <a
                className=Css.(
                  style([display(`inlineFlex), textDecoration(`none)])
                )
                target="_blank"
                href>
                <Text
                  textStyle={isSm ? TextStyles.mobileText : TextStyles.h4}
                  color=?{isSm ? None : Some(Colors.black)}>
                  {text |> React.string}
                </Text>
              </a>
            </Flex>
          )
       |> React.array}
    </Flex>
    <Flex mt={isSm ? `px(12) : `zero}>
      <DexterUi_ModeSwitch />
      <DexterUi_CurrencySwitch />
    </Flex>
  </Flex>;
};
