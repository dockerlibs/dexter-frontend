[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();

  <Flex
    alignItems=`center
    height={`px(61)}
    width={`percent(100.)}
    justifyContent=`center
    background=Colors.offWhite
    flexShrink=0.>
    <Flex
      maxWidth={`px(1024)}
      width={`percent(100.)}
      px={`px(isSm ? 24 : 28)}
      justifyContent=`spaceBetween
      alignItems=`center>
      <Flex
        alignItems=`center
        full=isSm
        justifyContent={isSm ? `spaceBetween : `flexStart}>
        <Flex mr={`px(18)}>
          <Flex height={`px(21)} width={`px(115)}>
            <DexterUi_Image src="logo.svg" />
          </Flex>
          <Flex ml={`px(14)} mt={`px(-2)}>
            <Text textStyle=TextStyles.version color=Colors.blackish1>
              {"V" ++ Common.packageVersionTruncated() |> React.string}
            </Text>
          </Flex>
        </Flex>
        <a
          className=Css.(style([textDecoration(`none)]))
          target="_blank"
          href="https://dexter.exchange">
          <DexterUi_Button px={`px(16)} variant=Secondary>
            {"About Dexter" |> React.string}
          </DexterUi_Button>
        </a>
      </Flex>
      <Flex>
        {!(isSm || Common.isMaintenanceMode())
         |> Utils.renderIf(<DexterUi_NavBarButtons />)}
        <DexterUi_ServiceUnavailable />
      </Flex>
    </Flex>
  </Flex>;
};
