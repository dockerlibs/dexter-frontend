include DexterUi_Style;

[@react.component]
let make = () =>
  Common.isMaintenanceMode()
    ? <DexterUi_MaintenanceMode />
    : <DexterUiContext> <DexterUi_Layout /> </DexterUiContext>;
