[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();
  let hasPositiveLqt = DexterUi_Hooks.useHasPositiveLqt();

  let (state, dispatch) =
    DexterUi_RemoveLiquidity_Reducer.useRemoveLiquidityReducer(
      balances |> Dexter_Balance.getTokens,
    );

  DexterUi_Hooks.useResetting(
    () => dispatch(ResetTokens),
    () => dispatch(ResetInputs),
  );

  switch (!isLoggedIn || !hasPositiveLqt, state.token) {
  | (true, _) => <DexterUi_RemoveLiquidityMessage />
  | (_, Some(token)) =>
    let (xtzAmount, tokenAmount) =
      switch (state.liquidity) {
      | Valid(liquidity, _) =>
        let (xtzAmount, tokenAmount) =
          Dexter_AddLiquidity.removeLiquidityXtzTokenOut(
            ~liquidityBurned=liquidity,
            ~totalLiquidity=token.exchangeTotalLqt,
            ~tokenPool=token.exchangeTokenBalance,
            ~xtzPool=token.exchangeXtz,
            (),
          );

        (xtzAmount, tokenAmount);
      | _ => (Tezos.Mutez.zero, Tezos.Token.zero)
      };

    <>
      <Flex mb={isSm ? `zero : `px(10)} flexDirection={isSm ? `column : `row}>
        <DexterUi_ExchangeColumn
          balance={ExchangeBalance(token)}
          title={"Redeem pool tokens:" |> React.string}
          isPoolToken=true
          tokenSearchBalances={balances |> Dexter_Balance.getPositiveLqt}
          tokenSearchOnChange={token =>
            switch (token) {
            | ExchangeBalance(token) =>
              dispatch(UpdatePoolToken(token));
              Dexter_Route.redirectToTokensHash(
                XtzBalance(Tezos.Mutez.zero),
                ExchangeBalance(token),
              );
            | _ => ()
            }
          }
          balanceInfo=[
            Tezos.Token.toStringWithCommas(token.lqtBalance),
            "Pool size: "
            ++ Tezos.Token.toStringWithCommas(token.exchangeTotalLqt),
          ]>
          <DexterUi_TokenInput
            limit={Token(token.lqtBalance)}
            withCurrencyValue=false
            token={ExchangeBalance(token)}
            onClickMax={_ =>
              dispatch(
                UpdateLiquidityValue(
                  Dexter_Value.getTokenValue(token.lqtBalance),
                ),
              )
            }
            updateValue={v => {
              let v: Valid.t(Tezos.Token.t) =
                switch (v) {
                | Valid(Token(token), s) => Valid(token, s)
                | Valid(Mutez(_), s)
                | Invalid(s) => Invalid(s)
                | InvalidInitialState => InvalidInitialState
                };
              dispatch(UpdateLiquidityValue(v));
            }}
            value={
              switch (state.liquidity) {
              | Valid(value, s) => Valid(Token(value), s)
              | Invalid(s) => Invalid(s)
              | InvalidInitialState => InvalidInitialState
              }
            }
          />
        </DexterUi_ExchangeColumn>
        <DexterUi_ExchangeSpacer />
        <Flex
          flexDirection=`column
          width={
            isSm ? `initial : `calc((`sub, `percent(200. /. 3.), `px(25)))
          }>
          <Flex flexDirection={isSm ? `column : `row}>
            <DexterUi_ExchangeColumn
              balance={ExchangeBalance(token)}
              title={"Receive (estimate):" |> React.string}>
              <DexterUi_ExchangeInput
                displayOnly=true
                note={
                  <DexterUi_CurrencyValue token value={Mutez(xtzAmount)} />
                }>
                {tokenAmount |> Tezos.Token.toStringWithCommas |> React.string}
              </DexterUi_ExchangeInput>
            </DexterUi_ExchangeColumn>
            <DexterUi_ExchangePlus />
            <DexterUi_ExchangeColumn balance={XtzBalance(xtzAmount)}>
              <DexterUi_ExchangeInput
                displayOnly=true
                note={<DexterUi_CurrencyValue value={Mutez(xtzAmount)} />}>
                {xtzAmount |> Tezos.Mutez.toTezStringWithCommas |> React.string}
              </DexterUi_ExchangeInput>
            </DexterUi_ExchangeColumn>
          </Flex>
          <DexterUi_MarketRate
            xtzPool={token.exchangeXtz}
            tokenPool={token.exchangeTokenBalance}
            symbol={token.symbol}
          />
        </Flex>
      </Flex>
      <DexterUi_RemoveLiquidityControls
        resetInputs={() => dispatch(ResetInputs)}
        token
        state
        tokenAmount
        xtzAmount
      />
    </>;
  | _ => React.null
  };
};
