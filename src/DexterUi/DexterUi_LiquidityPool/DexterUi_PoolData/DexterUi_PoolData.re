[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  let {darkMode} = DexterUiContext.Theme.useContext();
  let exchangeBalances = balances |> Dexter_Balance.getTokens1;

  <Flex flexDirection=`column mb={`px(-20)}>
    {!isSm |> Utils.renderIf(<DexterUi_PoolDataHeader />)}
    <Flex
      flexDirection=`column height={`px(367)} overflowY=`auto pb={`px(4)}>
      {exchangeBalances
       |> List.sort(
            (
              token1: Dexter_ExchangeBalance.t,
              token2: Dexter_ExchangeBalance.t,
            ) => {
            (token2.exchangeXtz |> Tezos.Mutez.toTezFloat)
            -. (token1.exchangeXtz |> Tezos.Mutez.toTezFloat)
            |> int_of_float
          })
       |> List.mapi((i, token) =>
            <Flex key={i |> string_of_int} flexDirection=`column flexShrink=0.>
              {isSm
                 ? <DexterUi_PoolDataRowMobile token />
                 : <DexterUi_PoolDataRow token />}
              {i < (exchangeBalances |> List.length)
               - 1
               |> Utils.renderIf(
                    <Flex
                      background={Colors.line(darkMode)}
                      full=true
                      height={`px(1)}
                      mt={`px(8)}
                    />,
                  )}
            </Flex>
          )
       |> Array.of_list
       |> React.array}
      <Flex height={`px(17)} />
      <a
        className=Css.(style([textDecoration(`none)]))
        href="https://stats.dexter.exchange"
        target="_blank">
        <Flex alignItems=`center justifyContent=`center>
          <DexterUi_Image
            className=Css.(style([height(`px(14)), marginRight(`px(6))]))
            src="pool-data.png"
          />
          <Text
            darkModeColor=Colors.primaryDark
            lightModeColor=Colors.blue
            textDecoration=`underline>
            {"View more pool data at stats.dexter.exchange" |> React.string}
          </Text>
        </Flex>
      </a>
    </Flex>
  </Flex>;
};
