[@react.component]
let make =
    (
      ~isHidden: bool,
      ~priceImpact: float,
      ~priceImpactConfirmed: bool,
      ~setPriceImpactConfirmed: (bool => bool) => unit,
    ) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {darkMode} = DexterUiContext.Theme.useContext();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();

  let variant: DexterUi_Panel.t =
    if (priceImpact < 1.) {
      Gray;
    } else if (priceImpact < 3.) {
      Blue;
    } else if (priceImpact < 5.) {
      Orange;
    } else {
      Red;
    };

  !(isSm && isHidden)
  |> Utils.renderIf(
       <Flex
         className=Css.(
           style([
             opacity(isHidden ? 0. : 1.),
             visibility(isHidden ? `hidden : `visible),
           ])
         )
         mt={`px(6)}
         justifyContent=`center>
         <DexterUi_Panel variant>
           <DexterUi_Tooltip
             text=
               {<>
                  {"The Price Impact of your desired trade will increase as the size of your trade increases."
                   |> React.string}
                </>}
             textAlign=`left
             width={`px(240)}>
             Common.iInCircle
           </DexterUi_Tooltip>
           <span
             className=Css.(style([marginLeft(`px(4)), fontWeight(bold)]))>
             {"Price impact: "
              ++ (priceImpact |> Format.truncateDecimals(2))
              ++ "%"
              |> React.string}
           </span>
           {(
              isLoggedIn
              && priceImpact > 5.
              && priceImpact < 50.
              && !priceImpactConfirmed
            )
            |> Utils.renderIf(
                 <>
                   {" - Are you sure you wish to continue? " |> React.string}
                   <Flex
                     className=Css.(
                       style([
                         borderBottom(
                           `px(1),
                           `solid,
                           darkMode ? Colors.offWhite : Colors.red,
                         ),
                       ])
                     )
                     mb={`px(-1)}
                     inlineFlex=true
                     onClick={_ => setPriceImpactConfirmed(_ => true)}>
                     <span className=Css.(style([fontSize(`px(10))]))>
                       {"Yes, continue." |> React.string}
                     </span>
                   </Flex>
                 </>,
               )}
           {priceImpact > 50.
            |> Utils.renderIf(
                 " - This exchange exceeds 50% price impact and cannot be executed."
                 |> React.string,
               )}
         </DexterUi_Panel>
       </Flex>,
     );
};
