[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  let (state, dispatch) =
    DexterUi_Exchange_Reducer.useExchangeReducer(balances);

  DexterUi_Hooks.useResetting(
    () => dispatch(ResetTokens),
    () => dispatch(ResetInputs),
  );

  let updateInputToken =
    React.useCallback1(
      (inputToken: Dexter.Balance.t) => {
        let outputToken =
          DexterUi_Exchange_Utils.getSecondToken(
            inputToken,
            state.inputToken,
            state.outputToken,
          );

        dispatch(UpdateTokens(inputToken, outputToken));
        Dexter_Route.redirectToTokensHash(inputToken, outputToken);
      },
      [|state.inputToken, state.outputToken|],
    );

  let updateOutputToken =
    React.useCallback1(
      (outputToken: Dexter.Balance.t) => {
        let inputToken =
          DexterUi_Exchange_Utils.getSecondToken(
            outputToken,
            state.outputToken,
            state.inputToken,
          );

        dispatch(UpdateTokens(inputToken, outputToken));
        Dexter_Route.redirectToTokensHash(inputToken, outputToken);
      },
      [|state.inputToken, state.outputToken|],
    );

  let onClickMax = {
    let inputValue =
      switch (state.inputToken, state.outputToken) {
      | (XtzBalance(xtz), ExchangeBalance(_))
          when Tezos.Mutez.(gtZero(userMax(xtz))) =>
        Some(Dexter_Value.getMutezInputValue(Tezos.Mutez.userMax(xtz)))
      | (ExchangeBalance(token), XtzBalance(_))
          when Tezos.Token.(gtZero(token.tokenBalance)) =>
        Some(Dexter_Value.getTokenInputValue(token.tokenBalance))
      | (ExchangeBalance(tokenInput), ExchangeBalance(_tokenOutput))
          when Tezos.Token.(gtZero(tokenInput.tokenBalance)) =>
        Some(Dexter_Value.getTokenInputValue(tokenInput.tokenBalance))
      | _ => None
      };

    inputValue
    |> Utils.map((inputValue, _) => dispatch(UpdateInputValue(inputValue)));
  };

  <Flex flexDirection=`column px={Css.px(16)} pt={Css.px(4)} flexGrow=1.>
    <Flex flexDirection={isSm ? `column : `row}>
      <DexterUi_ExchangeColumn
        balance={state.inputToken}
        minHeight={isSm ? `initial : `px(179)}
        title={"Exchange:" |> React.string}
        tokenSearchBalances=balances
        tokenSearchOnChange=updateInputToken
        balanceInfo=[
          state.inputToken |> Dexter_Balance.getAccountBalanceAsStringTruncated,
        ]>
        <DexterUi_TokenInput
          ?onClickMax
          token={state.inputToken}
          value={state.inputValue}
          updateValue={v => dispatch(UpdateInputValue(v))}
          xtzReserveNote={
            Tezos.Mutez.(requiredXtzReserve |> toTezStringWithCommas)
            ++ " XTZ must be reserved to pay network fees."
          }
        />
      </DexterUi_ExchangeColumn>
      <DexterUi_ExchangeSpacer
        onClick={_ => {
          dispatch(UpdateTokens(state.outputToken, state.inputToken));
          Dexter_Route.redirectToTokensHash(
            state.outputToken,
            state.inputToken,
          );
        }}
      />
      <DexterUi_ExchangeColumn
        balance={state.outputToken}
        title={"To receive (estimate):" |> React.string}
        tokenSearchSide=Side.Right
        tokenSearchBalances=balances
        tokenSearchOnChange=updateOutputToken
        balanceInfo=[
          state.outputToken
          |> Dexter_Balance.getAccountBalanceAsStringTruncated,
          "Dexter balance: "
          ++ (
            switch (state.inputToken, state.outputToken) {
            | (ExchangeBalance(_), XtzBalance(_)) =>
              state.inputToken |> Dexter_Balance.getExchangeTotalXtzAsString
            | (_, ExchangeBalance(_)) =>
              state.outputToken
              |> Dexter_Balance.getExchangeTotalTokenAsStringTruncated
            | _ => ""
            }
          ),
        ]>
        <DexterUi_TokenInput
          token={state.outputToken}
          value={state.outputValue}
          aboveLimitNote="There is insufficient liquidity for this exchange."
          updateValue={v => dispatch(UpdateOutputValue(v))}
          limit={
            switch (state.inputToken, state.outputToken) {
            | (ExchangeBalance(token), XtzBalance(_)) =>
              Mutez(token.exchangeXtz)
            | (_, ExchangeBalance(token)) =>
              Token(token.exchangeTokenBalance)
            | (_, XtzBalance(xtz)) => Mutez(xtz)
            }
          }
        />
      </DexterUi_ExchangeColumn>
    </Flex>
    <DexterUi_ExchangeControls
      state
      resetInputs={() => dispatch(ResetInputs)}
    />
  </Flex>;
};
