[@react.component]
let make = (~token: Dexter_ExchangeBalance.t) =>
  "XTZ/" ++ token.symbol |> React.string;
