[@react.component]
let make =
    (
      ~text: option(string)=?,
      ~subText: option(string)=?,
      ~loaderColor: option(Css.Types.Color.t)=?,
      ~loaderSize: int=48,
    ) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  let loaderColor =
    loaderColor
    |> Utils.orDefault(darkMode ? Colors.primaryDark : Colors.blue);

  let loaderBorderWidth = Js.Math.ceil((loaderSize |> float_of_int) /. 10.);

  <Flex flexDirection=`column alignItems=`center>
    <Flex
      width={`px(loaderSize)}
      height={`px(loaderSize)}
      className=Css.(
        style([
          selector(
            "div",
            [
              boxSizing(`borderBox),
              display(`block),
              position(`absolute),
              width(px(loaderSize)),
              height(px(loaderSize)),
              border(px(loaderBorderWidth), `solid, `transparent),
              borderRadius(pct(50.)),
              borderTopColor(loaderColor),
              animation(
                ~duration=1200,
                ~timingFunction=cubicBezier(0.5, 0., 0.5, 1.),
                ~iterationCount=`infinite,
                keyframes([
                  (0, [transform(rotate(deg(0.)))]),
                  (100, [transform(rotate(deg(360.)))]),
                ]),
              ),
              nthChild(`n(1), [animationDelay(-450)]),
              nthChild(`n(2), [animationDelay(-300)]),
              nthChild(`n(3), [animationDelay(-150)]),
              // animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            ],
          ),
        ])
      )>
      <div />
      <div />
      <div />
      <div />
    </Flex>
    {text
     |> Utils.renderOpt(text =>
          <Text
            className=Css.(style([marginTop(`px(loaderSize / 3))]))
            textStyle=TextStyles.h2
            lightModeColor=Colors.grey1
            fontWeight=`bold>
            {text |> React.string}
          </Text>
        )}
    {subText
     |> Utils.renderOpt(subText =>
          <Text textStyle=TextStyles.h2 lightModeColor=Colors.grey1>
            {subText |> React.string}
          </Text>
        )}
  </Flex>;
};
