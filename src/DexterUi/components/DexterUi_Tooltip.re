[@react.component]
let make =
    (
      ~children: React.element,
      ~text: React.element,
      ~textAlign: option(Text.textAlignType)=?,
      ~width: Flex.widthType,
    ) => {
  <Flex
    className=Css.(
      style([
        cursor(`default),
        hover([
          selector(".popover", [opacity(1.), visibility(`visible)]),
        ]),
      ])
    )
    inlineFlex=true
    position=`relative>
    <Flex
      className=Css.(
        merge([
          style([
            transform(translate(pct(-50.), pct(-100.))),
            opacity(0.),
            visibility(`hidden),
          ]),
          "popover",
        ])
      )
      left={`percent(50.)}
      position=`absolute
      top=`zero
      maxWidth=Css.initial
      pb={`px(12)}>
      <Flex
        background=Colors.white
        borderRadius={`px(8)}
        p={`px(12)}
        width
        className=Css.(
          style([
            boxShadow(
              Shadow.box(
                ~y=px(2),
                ~blur=px(10),
                rgba(0, 0, 0, `num(0.25)),
              ),
            ),
            before([
              position(`absolute),
              left(pct(50.)),
              bottom(px(13)),
              contentRule(`text("")),
              transform(translate(pct(-50.), pct(100.))),
              border(px(10), `solid, `transparent),
              borderTopColor(Colors.white),
              borderBottomStyle(`none),
            ]),
          ])
        )>
        <Text darkModeColor=Colors.grey lineHeight={`px(15)} ?textAlign>
          text
        </Text>
      </Flex>
    </Flex>
    children
  </Flex>;
};
