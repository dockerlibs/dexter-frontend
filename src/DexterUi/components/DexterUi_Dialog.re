[@react.component]
let make =
    (
      ~background=Colors.offWhite,
      ~children: React.element,
      ~onClose: unit => unit,
      ~px=`px(18),
      ~width: Flex.widthType,
      ~withPaddingBottom: bool=false,
    ) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();

  <>
    <Flex
      position=`fixed
      background={`rgba((0, 0, 0, `num(0.33)))}
      width={`percent(100.)}
      height={`percent(100.)}
      top=`zero
      left=`zero
      zIndex=4
      onClick={_ => onClose()}
    />
    <Flex
      alignItems=`center
      background
      borderRadius={`px(16)}
      className=Css.(
        style([
          transform(
            isSm
              ? translate(pct(-50.), pct(-50.)) : translateX(pct(-50.)),
          ),
        ])
      )
      left={`percent(50.)}
      top={isSm ? `percent(50.) : `px(108)}
      flexDirection=`column
      maxHeight={`calc((`sub, `vh(100.), `px(isSm ? 100 : 260)))}
      position=`fixed
      pb={withPaddingBottom ? `px(24) : `zero}
      pt={`px(24)}
      px
      width
      maxWidth={`calc((`sub, `vw(100.), `px(32)))}
      zIndex=5>
      <Flex
        position=`absolute
        top={`px(isSm ? 16 : 22)}
        right={`px(isSm ? 16 : 22)}
        onClick={_ => onClose()}>
        <DexterUi_Icon name="close" />
      </Flex>
      children
    </Flex>
  </>;
};
