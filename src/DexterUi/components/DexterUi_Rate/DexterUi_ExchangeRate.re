let xtzToTokenRate = (inputValue: Valid.t(InputType.t), xtz, token) =>
  switch (inputValue) {
  | Valid(Mutez(inputValue), _) when Tezos.Mutez.gtZero(inputValue) =>
    Dexter_Exchange.xtzToTokenExchangeRateForDisplay(inputValue, xtz, token)
  | _ => Dexter_Exchange.xtzToTokenMarketRateForDisplay(xtz, token)
  };

let tokenToXtzRate = (inputValue: Valid.t(InputType.t), xtz, token) =>
  switch (inputValue) {
  | Valid(Token(inputValue), _) when Tezos.Token.gtZero(inputValue) =>
    Dexter_Exchange.tokenToXtzExchangeRateForDisplay(inputValue, xtz, token)
  | _ => Dexter_Exchange.tokenToXtzMarketRateForDisplay(xtz, token)
  };

let tokenToTokenRate =
    (
      inputValue: Valid.t(InputType.t),
      inputToken: Dexter.ExchangeBalance.t,
      outputToken: Dexter.ExchangeBalance.t,
    ) =>
  switch (inputValue) {
  | Valid(Token(inputValue), _) when Tezos.Token.gtZero(inputValue) =>
    Dexter_Exchange.tokenToTokenExchangeRateForDisplay(
      inputValue,
      inputToken.exchangeXtz,
      inputToken.exchangeTokenBalance,
      outputToken.exchangeXtz,
      outputToken.exchangeTokenBalance,
    )
  | _ =>
    Dexter_Exchange.tokenToTokenMarketRateForDisplay(
      inputToken.exchangeXtz,
      inputToken.exchangeTokenBalance,
      outputToken.exchangeXtz,
      outputToken.exchangeTokenBalance,
    )
  };

/** outputToken should never be XTZ */
let display =
    (
      inputValue: Valid.t(InputType.t),
      inputToken: Dexter_Balance.t,
      outputToken: Dexter_Balance.t,
    ) => {
  let lowestValue = Common.getLowestValueWithDecimalsFloat(6);

  switch (inputToken, outputToken) {
  | (XtzBalance(_), ExchangeBalance(outputToken))
      when Tezos.Mutez.gtZero(outputToken.exchangeXtz) =>
    let rate =
      xtzToTokenRate(
        inputValue,
        outputToken.exchangeXtz,
        outputToken.exchangeTokenBalance,
      );

    let sign = rate < lowestValue ? " < " : " = ";
    let rate = rate |> Format.truncateDecimals(~sign=false, 6);

    "1 XTZ" ++ sign ++ rate ++ " " ++ outputToken.symbol;
  | (ExchangeBalance(inputToken), XtzBalance(_))
      when Tezos.Token.gtZero(inputToken.exchangeTokenBalance) =>
    let rate =
      tokenToXtzRate(
        inputValue,
        inputToken.exchangeXtz,
        inputToken.exchangeTokenBalance,
      );

    let sign = rate < lowestValue ? " < " : " = ";
    let rate = rate |> Format.truncateDecimals(~sign=false, 6);

    "1 " ++ inputToken.symbol ++ sign ++ rate ++ " XTZ";
  | (ExchangeBalance(inputToken), ExchangeBalance(outputToken))
      when
        Tezos.Token.gtZero(inputToken.exchangeTokenBalance)
        && Tezos.Token.gtZero(outputToken.exchangeTokenBalance) =>
    let rate = tokenToTokenRate(inputValue, inputToken, outputToken);

    let sign = rate < lowestValue ? " < " : " = ";
    let rate = rate |> Format.truncateDecimals(~sign=false, 6);

    "1 " ++ inputToken.symbol ++ sign ++ rate ++ " " ++ outputToken.symbol;

  | (XtzBalance(_), ExchangeBalance(token)) =>
    "The " ++ token.symbol ++ " exchange has no liquidity"
  | _ => "No exchange rate"
  };
};

[@react.component]
let make =
    (
      ~inputValue: Valid.t(InputType.t),
      ~inputToken: Dexter_Balance.t,
      ~outputToken: Dexter_Balance.t,
    ) =>
  <DexterUi_Rate>
    {display(inputValue, inputToken, outputToken) |> React.string}
  </DexterUi_Rate>;
