[@react.component]
let make = (~icon: string, ~iconDark: option(string)=?, ~small: bool=false) => {
  let {darkMode} = DexterUiContext.Theme.useContext();
  let size = `px(small ? 24 : 36);

  <Flex flexShrink=0. width=size height=size position=`relative>
    <DexterUi_Image
      className=Css.(style([width(pct(100.))]))
      src={
        switch (darkMode, iconDark) {
        | (true, Some(iconDark)) => iconDark
        | _ => icon
        }
      }
    />
  </Flex>;
};
