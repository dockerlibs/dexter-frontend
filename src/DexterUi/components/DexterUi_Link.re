[@react.component]
let make =
    (
      ~children: React.element,
      ~fontSize=?,
      ~ellipsis: bool=true,
      ~href: string,
    ) =>
  <Text
    ?fontSize
    darkModeColor=Colors.primaryDark
    lightModeColor=Colors.blue
    ellipsis>
    <a href target="_blank"> children </a>
  </Text>;
