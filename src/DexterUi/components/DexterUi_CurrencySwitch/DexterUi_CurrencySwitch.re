[@react.component]
let make = () => {
  let {currentCurrency, currencies} = DexterUiContext.Currencies.useContext();
  let (showDialog, setShowDialog) = React.useState(_ => false);

  let isUnavailable =
    DexterUiContext.Services.isUnavailable(Service.CoinGecko);

  <>
    {showDialog
     |> Utils.renderIf(
          <DexterUi_CurrencySwitchList
            onClose={_ => setShowDialog(_ => false)}
          />,
        )}
    <Flex
      background=Colors.lightGrey1
      borderRadius={`px(14)}
      alignItems=`center
      justifyContent=`spaceBetween
      p={`px(4)}
      ml={`px(12)}
      onClick=?{
        currencies |> List.length > 0 || isUnavailable
          ? Some(_ => setShowDialog(_ => true)) : None
      }
      position=`relative>
      <Flex
        alignItems=`center
        background=Colors.offWhite
        borderRadius={`px(14)}
        height={`percent(100.)}
        justifyContent=`center
        py={`px(2)}
        width={`px(36)}>
        <Text
          textStyle=TextStyles.h4
          fontWeight=`semiBold
          textDecoration={isUnavailable ? `lineThrough : `none}
          color={isUnavailable ? Colors.red : Colors.text(false)}>
          {currentCurrency
           |> Utils.map((currency: Currency.t) => currency.symbol)
           |> Utils.orDefault(Dexter_LocalStorage.Currency.get())
           |> React.string}
        </Text>
      </Flex>
    </Flex>
  </>;
};
