[@react.component]
let make = (~onClose: unit => unit) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let (value, setValue) = React.useState(_ => "");
  let isUnavailable =
    DexterUiContext.Services.isUnavailable(Service.CoinGecko);

  <DexterUi_Dialog
    onClose
    px={`px(isSm ? 20 : 24)}
    width={`px(366)}
    withPaddingBottom=isUnavailable>
    <DexterUi_Message
      title={"Select local currency" |> React.string}
      subtitle=?{
        isUnavailable
          ? Some(
              <>
                {"Pricing data is currently unavailable." |> React.string}
                <br />
                {"Please check back again soon." |> React.string}
              </>,
            )
          : None
      }
      darkModeEnabled=false
    />
    {!isUnavailable
     |> Utils.renderIf(
          <>
            <Flex mt={`px(12)} />
            <input
              autoFocus=true
              placeholder="Search..."
              className=Css.(
                style([
                  background(Colors.white),
                  border(`zero, `none, Colors.white),
                  fontSize(px(10)),
                  padding2(~v=`zero, ~h=`px(15)),
                  boxSizing(`borderBox),
                  borderRadius(`px(16)),
                  height(`px(32)),
                  Css.placeholder([color(Colors.grey)]),
                  Css.width(pct(100.)),
                  flexShrink(0.),
                ])
              )
              value
              onChange={ev => setValue(ev |> Common.eventToValue)}
            />
            <Flex
              flexDirection=`column
              full=true
              maxHeight={`px(368)}
              mt={`px(8)}
              overflowY=`auto
              pb={`px(14)}>
              <DexterUi_CurrencySwitchListGroup popular=true value />
              <DexterUi_CurrencySwitchListGroup value />
            </Flex>
          </>,
        )}
  </DexterUi_Dialog>;
};
