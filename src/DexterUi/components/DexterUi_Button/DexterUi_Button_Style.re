open Css;

let buttonBase =
  style([
    textAlign(`center),
    whiteSpace(`nowrap),
    alignItems(`center),
    fontWeight(`semiBold),
  ]);

let primary = (darkMode: bool, small: bool) =>
  merge([
    style([
      color(Colors.white),
      background(darkMode ? Colors.primaryDark : Colors.primary),
      hover([background(darkMode ? hex("566390") : hex("647796"))]),
      active([background(darkMode ? hex("566390") : hex("647796"))]),
      disabled([background(darkMode ? Colors.primaryDark : Colors.primary)]),
      height(px(small ? 32 : 40)),
      borderRadius(px(20)),
      fontSize(px(small ? 11 : 13)),
    ]),
    buttonBase,
  ]);

let secondary =
  merge([
    style([
      border(pxFloat(1.5), `solid, hex("26325C")),
      color(hex("26325C")),
      hover([borderColor(hex("647796")), color(hex("647796"))]),
      active([borderColor(hex("647796")), color(hex("647796"))]),
      disabled([borderColor(hex("26325C")), color(hex("26325C"))]),
      paddingTop(px(8)),
      paddingBottom(px(8)),
      borderRadius(px(20)),
      fontSize(px(11)),
    ]),
    buttonBase,
  ]);
