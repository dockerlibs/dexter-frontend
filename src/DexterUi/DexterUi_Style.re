open Css;

global(
  "body",
  [
    margin(`zero),
    media(
      "(min-width: 1180px) and (min-height: 724px)",
      [unsafe("zoom", "1.15"), maxHeight(vh(100.)), overflowY(`hidden)],
    ),
  ],
);
global("button", [padding(`zero)]);
global(
  "html",
  [
    background(Colors.offWhite),
    media("(max-device-width: 768px)", [background(Colors.white)]),
  ],
);
global("input", [outline(`zero, `solid, Colors.white)]);
global("*", [fontFamily(`custom("Source Sans Pro"))]);
global("::-webkit-scrollbar", [height(`zero), width(`zero)]);
global(
  ".beacon-modal__wrapper",
  [
    media(
      "(max-width: 532px)",
      [
        important(maxWidth(`calc((`sub, `vw(100.), `px(32))))),
        important(padding(`px(16))),
      ],
    ),
  ],
);
