let useHasInsufficientXtz = () => {
  let {account}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  account
  |> Utils.map((account: Dexter_Account.t) =>
       Tezos.Mutez.(leqZero(userMax(account.xtz)))
     )
  |> Utils.orDefault(false);
};

let useTokensFromLocalStorage =
    (balances: list(Dexter_Balance.t))
    : (option(Dexter_Balance.t), option(Dexter_Balance.t)) => {
  let symbols =
    Dexter_LocalStorage.SelectedTokens.get()
    |> Utils.orDefault("")
    |> String.split_on_char('_');

  (
    List.nth_opt(symbols, 0)
    |> Utils.flatMap(symbol =>
         balances |> Dexter_Balance.findBalanceBySymbol(symbol)
       ),
    List.nth_opt(symbols, 1)
    |> Utils.flatMap(symbol =>
         balances |> Dexter_Balance.findBalanceBySymbol(symbol)
       ),
  );
};

let useHasPositiveLqt = () => {
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  React.useMemo1(
    () => balances |> Dexter_Balance.getPositiveLqt |> List.length > 0,
    [|balances|],
  );
};

let useHasPositiveBalances = () => {
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  React.useMemo1(
    () => balances |> Dexter_Balance.getPositive |> List.length > 0,
    [|balances|],
  );
};

let useIsLoggedIn = () => {
  let {account}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  React.useMemo1(() => account |> Belt.Option.isSome, [|account|]);
};

let useIsTransactionPending = () => {
  let {transactionStatus} = DexterUiContext.Transactions.useContext();
  React.useMemo1(() => transactionStatus === Pending, [|transactionStatus|]);
};

let useResetting = (resetTokens: unit => unit, resetInputs: unit => unit) => {
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  let isLoggedIn = useIsLoggedIn();
  /* monitor the balances prop, if it changes, update the tokens */
  React.useEffect1(
    () => {
      resetTokens();
      None;
    },
    [|balances|],
  );

  /* monitor the account prop, if it changes, reset inputs */
  React.useEffect1(
    () => {
      resetInputs();
      None;
    },
    [|isLoggedIn|],
  );
};
