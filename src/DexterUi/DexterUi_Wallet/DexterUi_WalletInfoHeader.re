[@react.component]
let make = (~account: Dexter_Account.t) => {
  let {isXxs} = DexterUiContext.Responsive.useDevice();
  let {disconnect}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  <Flex
    justifyContent=`spaceBetween
    alignItems=`flexStart
    flexDirection={isXxs ? `column : `row}>
    <Flex flexDirection=`column mr={`px(20)} mb={isXxs ? `px(10) : `zero}>
      <Flex alignItems=`center>
        <Flex
          width={`px(6)}
          height={`px(6)}
          borderRadius={`percent(50.)}
          background=Colors.green
          mr={`px(4)}
        />
        <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
          {"Connected wallet address" |> React.string}
        </Text>
      </Flex>
      <Text> {Tezos.Address.toString(account.address) |> React.string} </Text>
    </Flex>
    <DexterUi_Button
      onClick={_ => disconnect()}
      px={`px(16)}
      small=true
      width=?{isXxs ? Some(`percent(100.)) : None}>
      {"Disconnect" |> React.string}
    </DexterUi_Button>
  </Flex>;
};
