type t = {
  darkMode: bool,
  setDarkMode: bool => unit,
};

let defaultValue = {darkMode: false, setDarkMode: _ => ()};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (darkMode, setDarkMode) =
      React.useState(_ => Dexter_LocalStorage.DarkMode.get());

    let setDarkMode =
      React.useCallback(darkMode => {
        Dexter_LocalStorage.DarkMode.set(darkMode);
        setDarkMode(_ => darkMode);
      });

    <ProviderInternal value={darkMode, setDarkMode}>
      children
    </ProviderInternal>;
  };
};

let useContext = (): t => React.useContext(ctx);
