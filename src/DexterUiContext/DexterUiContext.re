[@react.component]
let make = (~children: React.element) => {
  <DexterUiContext_Services.Provider>
    <DexterUiContext_Theme.Provider>
      <DexterUiContext_Route.Provider>
        <DexterUiContext_Currencies.Provider>
          <DexterUiContext_Account.Provider>
            <DexterUiContext_Transactions.Provider>
              <DexterUiContext_Responsive.Provider>
                children
              </DexterUiContext_Responsive.Provider>
            </DexterUiContext_Transactions.Provider>
          </DexterUiContext_Account.Provider>
        </DexterUiContext_Currencies.Provider>
      </DexterUiContext_Route.Provider>
    </DexterUiContext_Theme.Provider>
  </DexterUiContext_Services.Provider>;
};

module Account = DexterUiContext_Account;
module Currencies = DexterUiContext_Currencies;
module Route = DexterUiContext_Route;
module Theme = DexterUiContext_Theme;
module Transactions = DexterUiContext_Transactions;
module Services = DexterUiContext_Services;
module Responsive = DexterUiContext_Responsive;
