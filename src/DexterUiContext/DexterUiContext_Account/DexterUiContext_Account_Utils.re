let setBalancesFromDexter = (setBalances: list(Dexter_Balance.t) => unit) => {
  Dexter_Query.getBalances()
  |> Js.Promise.then_(balances => {
       switch (balances |> Array.to_list |> Common.sequence) {
       | Belt.Result.Ok((balances: list(Dexter_ExchangeBalance.t))) =>
         setBalances(
           balances
           |> List.map(balance => Dexter_Balance.ExchangeBalance(balance))
           |> List.append([Dexter_Balance.XtzBalance(Tezos_Mutez.zero)]),
         )
       | _ => ()
       };

       Js.Promise.resolve();
     })
  |> Js.Promise.catch(error => {
       Js.log(error);
       Common.reload();
       Js.Promise.resolve();
     })
  |> ignore;
};

let rec loginToBeacon =
        (client: Beacon.dappClient, firstAttempt: bool)
        : Js.Promise.t(option((Beacon.dappClient, Beacon.AccountInfo.t))) => {
  Beacon.getActiveAccount(client)
  |> Js.Promise.then_(account =>
       switch (account) {
       | None =>
         if (firstAttempt) {
           Beacon.requestPermissions(client, Dexter_Settings.beaconNetwork)
           |> Js.Promise.then_(_permissions => {
                /* user has accepted the permissions but we need to check if they are the correct ones */
                loginToBeacon(
                  client,
                  false,
                )
              });
         } else {
           Js.Promise.resolve(None);
         }

       | Some((account: Beacon.AccountInfo.t)) =>
         if (Beacon.hasRequiredPermissions(account)) {
           Js.Promise.resolve(Some((client, account)));
         } else if (firstAttempt) {
           Beacon.requestPermissions(client, Dexter_Settings.beaconNetwork)
           |> Js.Promise.then_(_permissions => {loginToBeacon(client, false)});
         } else {
           Js.Promise.resolve(None);
         }
       }
     );
};

let connectToBeacon =
    (
      setAccount: option(Dexter_Account.t) => unit,
      setBalances: list(Dexter_Balance.t) => unit,
    ) => {
  let client = Beacon.createDappClient("Dexter");
  loginToBeacon(client, true)
  |> Js.Promise.then_(
       (response: option((Beacon.dappClient, Beacon.AccountInfo.t))) =>
       switch (response) {
       | Some((client, account)) =>
         Dexter_Account.fetch(
           Common.unwrapResult(Tezos.Address.ofString(account.address)),
           Tezos_Wallet.Beacon(client),
         )
         |> Js.Promise.then_(((account, balances)) => {
              setAccount(account);
              setBalances(balances);
              Js.Promise.resolve();
            })
       | None =>
         setBalancesFromDexter(setBalances);
         Js.log("Unable to get beacon client or account.")
         |> Js.Promise.resolve;
       }
     )
  |> ignore;
  Js.Promise.resolve();
};

let useSetup =
    (
      connectToBeacon: unit => unit,
      setBalances: list(Dexter_Balance.t) => unit,
    ) =>
  React.useEffect0(() => {
    switch (Dexter_LocalStorage.WalletType.get()) {
    | Some("Beacon") => connectToBeacon()
    | _ => setBalancesFromDexter(setBalances)
    };

    None;
  });

let accountPollingIntervalId: ref(option(Js.Global.intervalId)) = ref(None);
let accountPollingInterval = 2 * 60 * 1000; // 2 minutes

let refetchAccount =
    (
      ~account: option(Dexter_Account.t),
      ~setAccount: option(Dexter_Account.t) => unit,
      ~setBalances: list(Dexter_Balance.t) => unit,
      ~bypassCache: bool=false,
      (),
    ) => {
  if (bypassCache) {
    accountPollingIntervalId^
    |> Utils.map(intervalId => intervalId |> Js.Global.clearInterval)
    |> Utils.orDefault();
  };

  account
  |> Utils.map((account: Dexter_Account.t) =>
       Dexter_Account.fetch(account.address, account.wallet)
       |> Js.Promise.then_(((account, balances)) => {
            setAccount(account);
            setBalances(balances);
            Js.Promise.resolve();
          })
       |> ignore
     )
  |> Utils.orDefault();
};

let useAccountPolling =
    (account: option(Dexter_Account.t), refetchAccount: bool => unit) => {
  React.useEffect1(
    () => {
      accountPollingIntervalId :=
        Some(
          Js.Global.setInterval(
            () => refetchAccount(false),
            accountPollingInterval,
          ),
        );

      accountPollingIntervalId^
      |> Utils.map((intervalId, ()) => intervalId |> Js.Global.clearInterval);
    },
    [|account|],
  );
};
