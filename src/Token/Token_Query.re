/**
 * Types and functions for querying data from an FA1.2 token and encoding data
 * for FA1.2 operations.
 */

type response = {
  accountTokenBalance: Tezos.Token.t,
  dexterAllowanceForAccount: Tezos.Token.t,
  dexterBigMap: Dexter_BigMap.t,
};

let fetch =
    (url: string, account: Tezos.Address.t, exchange: Dexter_Exchange.t)
    : Js.Promise.t(Belt.Result.t(response, string)) => {
  let balancePromise =
    BakingBad.getTokenBalances(
      /* ~decimals=exchange.decimals, */
      /* ~contract=exchange.tokenContract, */
      account,
    );

  let dexterBigMapPromise =
    account
    |> Tezos.Address.toScriptExpr
    |> Tezos.RPC.getBigMapValue(url, "main", "head", exchange.dexterBigMapId)
    |> Js.Promise.then_(result => {
         switch (result) {
         | Belt.Result.Ok(Some(expression)) =>
           Dexter_BigMap.ofExpression(expression) |> Js.Promise.resolve

         | Belt.Result.Ok(None) =>
           /* the account does not have any lqt, set it to zero */
           //  Js.log("The account does not have any liquidity.");
           Belt.Result.Ok(
             {lqt: Tezos.Token.mkToken(Bigint.zero, 0)}: Dexter_BigMap.t,
           )
           |> Js.Promise.resolve
         | Belt.Result.Error(_err) =>
           Belt.Result.Ok(
             {lqt: Tezos.Token.mkToken(Bigint.zero, 0)}: Dexter_BigMap.t,
           )
           |> Js.Promise.resolve
         /* Js.log2("Token_Query.fetch.bigMapPromise failed", err); */
         /* Belt.Result.Error("bigMapPromise failed: " ++ err) */
         /* |> Js.Promise.resolve; */
         }
       });

  Js.Promise.all2((dexterBigMapPromise, balancePromise))
  |> Js.Promise.then_(results =>
       switch (results) {
       | (Belt.Result.Ok(dexterBigMap), accountTokenBalanceResult) =>
         let accountTokenBalance =
           switch (accountTokenBalanceResult) {
           | Belt.Result.Ok(balances) =>
             switch (
               Belt.Map.String.get(
                 balances,
                 Tezos.Contract.toString(exchange.tokenContract),
               )
             ) {
             | Some(balance) =>
               switch (Tezos.Token.ofBigint(balance, exchange.decimals)) {
               | Belt.Result.Ok(balance) => balance
               | _ => Tezos.Token.zero
               }

             | _ => Tezos.Token.zero
             }
           | _ => Tezos.Token.zero
           };

         Belt.Result.Ok({
           accountTokenBalance,
           dexterBigMap,
           dexterAllowanceForAccount: Tezos.Token.zero,
         })
         |> Js.Promise.resolve;
       | (Belt.Result.Error(error), _) =>
         Js.log("Token.fetch failed: " ++ error);
         Belt.Result.Error("Token.fetch failed: " ++ error)
         |> Js.Promise.resolve;
       }
     );
};

let encodeApprove = (spender: string, value: Tezos.Token.t) =>
  Tezos.(
    Expression.SingleExpression(
      Primitives.PrimitiveData(PrimitiveData.Pair),
      Some([
        Expression.StringExpression(spender),
        Expression.IntExpression(value.value),
      ]),
      None,
    )
  );
