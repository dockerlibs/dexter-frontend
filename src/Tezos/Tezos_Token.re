/**
 * A type that represents a tokens numerical value
 * the number of decimals a token has is generally ignore for calculations
 * but used for rendering to users
 */

type t = {
  value: Bigint.t,
  decimals: int,
};

let mkToken = (value, decimals) => {value, decimals};

/**
 * common values
 */

let zero = mkToken(Bigint.zero, 0);

let one = mkToken(Bigint.one, 0);

let succ = t => {...t, value: Bigint.add(t.value, Bigint.one)};

/**
 * of conversions
 */

let ofBigint = (int: Bigint.t, decimals: int): Belt.Result.t(t, string) =>
  if (Bigint.compare(int, Bigint.zero) >= 0) {
    Belt.Result.Ok(mkToken(int, decimals));
  } else {
    Belt.Result.Error("ofBigint expected a non-negative bigint value.");
  };

let ofInt64 = (int: Int64.t, decimals: int): Belt.Result.t(t, string) =>
  if (Int64.compare(int, Int64.zero) >= 0) {
    Belt.Result.Ok(mkToken(Bigint.of_int64(int), decimals));
  } else {
    Belt.Result.Error("ofInt64 expected a non-negative int64 value.");
  };

let ofFloat = (f: float, decimals): t => {
  let s = Js.Float.toString(f);
  let a = Js.String.split(".", s);

  if (Array.length(a) == 1) {
    {value: Bigint.of_float(f), decimals};
  } else if (Array.length(a) == 2) {
    let leftSide = a[0];
    let rightSide = a[1];
    let rightSideLength = String.length(rightSide);

    let rr =
      if (rightSideLength == decimals) {
        rightSide;
      } else if (rightSideLength < decimals) {
        rightSide ++ Tezos_Util.repeatString(decimals - rightSideLength, "0");
      } else {
        String.sub(rightSide, 0, decimals);
      };

    {value: Bigint.of_string(leftSide ++ rr), decimals};
  } else {
    {value: Bigint.zero, decimals};
  };
};

// this expects decimaled form
// {value: 100, decimals: 1}: "10"
// {value: 100, decimals: 1}: "10.0"
// {value: 54789, decimals: 3}: "54.789"
let ofString = (string: string, decimals: int): Belt.Result.t(t, string) => {
  let a = Js.String.split(".", string);

  let invalidCharsRegex = [%bs.re "/[^0-9.]/g"];
  let containsInvalidChars = string |> Js.Re.test_(invalidCharsRegex);
  let tooManyDecimals = Common.getDecimalsCount(string) > decimals;
  let tooManyDots = Array.length(a) > 2;

  if (containsInvalidChars || tooManyDecimals || tooManyDots) {
    Belt.Result.Error(string);
  } else if (Array.length(a) == 1) {
    Belt.Result.Ok({
      value: Bigint.(mul(of_string(string), pow(of_int(10), decimals))),
      decimals,
    });
  } else {
    let leftSide = a[0];
    let rightSide = a[1];
    let rightSideLength = String.length(rightSide);

    let rr =
      if (rightSideLength == decimals) {
        rightSide;
      } else if (rightSideLength < decimals) {
        rightSide ++ Tezos_Util.repeatString(decimals - rightSideLength, "0");
      } else {
        String.sub(rightSide, 0, decimals);
      };

    Belt.Result.Ok({value: Bigint.of_string(leftSide ++ rr), decimals});
  };
};

/**
 * to conversions
 */

let toInt64 = (t: t): Int64.t => Bigint.to_int64(t.value);

let toBigint = (t: t): Bigint.t => t.value;

/**
 * arithmetic
 */

let add = (x, y) => {
  value: Bigint.add(x.value, y.value),
  decimals: max(x.decimals, y.decimals),
};

let sub = (x, y) => {
  value: Bigint.sub(x.value, y.value),
  decimals: max(x.decimals, y.decimals),
};

let mul = (x, y) => {
  value: Bigint.mul(x.value, y.value),
  decimals: max(x.decimals, y.decimals),
};

let div = (x, y) => {
  value: Bigint.div(x.value, y.value),
  decimals: max(x.decimals, y.decimals),
};

let cdiv = (x, y) => {
  value: Bigint.cdiv(x.value, y.value),
  decimals: max(x.decimals, y.decimals),
};

/**
 * comparison
 */

let compare = (x, y) => Bigint.compare(x.value, y.value);

let equal = (x, y) => compare(x, y) == 0;

let leq = (x, y) => compare(x, y) < 1;

let geq = (x, y) => compare(x, y) > (-1);

let gt = (x, y) => compare(x, y) > 0;

let lt = (x, y) => compare(x, y) < 0;

let leqZero = x => compare(x, zero) <= 0;

let geqZero = x => compare(x, zero) >= 0;

let ltZero = x => compare(x, zero) < 0;

let gtZero = x => compare(x, zero) > 0;

let eqZero = x => compare(x, zero) === 0;

let min = (x, y) =>
  if (compare(x, y) < 0) {
    y;
  } else {
    x;
  };

let max = (x, y) =>
  if (compare(x, y) < 0) {
    x;
  } else {
    y;
  };

let subPointThreePercent = x => {
  div(
    mul(
      mul(x, mkToken(Bigint.of_int(1000), 0)),
      mkToken(Bigint.of_int(997), 0),
    ),
    mkToken(Bigint.of_int(1000000), 0),
  );
};

let addPointThreePercent = x => {
  div(
    mul(
      mul(x, mkToken(Bigint.of_int(1000), 0)),
      mkToken(Bigint.of_int(1003), 0),
    ),
    mkToken(Bigint.of_int(1000000), 0),
  );
};

/**
 * JSON encoding and decoding
 */
let encode = (t: t): Js.Json.t =>
  Json.Encode.(
    object_([
      ("value", Tezos_Util.bigintEncode(t.value)),
      ("decimals", int(t.decimals)),
    ])
  );

let decode = (json: Js.Json.t) =>
  switch (
    Json.Decode.{
      value:
        field(
          "value",
          a => Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a)),
          json,
        ),
      decimals: field("decimals", int, json),
    }
  ) {
  | v => Belt.Result.Ok(v)
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Token.decode failed: " ++ error)
  };

let decodeFromString = (decimals: int, json: Js.Json.t) =>
  switch (
    {
      value: Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(json)),
      decimals,
    }
  ) {
  | v => Belt.Result.Ok(v)
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Token.decode failed: " ++ error)
  };

// this produces the decimaled form
// {value: 100, decimals: 1}: "10"
// {value: 100, decimals: 1}: "10.0"
// {value: 54789, decimals: 3}: "54.789"
let toString = (t: t): string => {
  let s = Bigint.to_string(t.value);
  let sl = String.length(s);

  if (t.decimals == 0 || s === "0") {
    s;
  } else {
    (
      sl <= t.decimals
        ? "0." ++ ("0" |> Tezos_Util.repeatString(t.decimals - sl)) ++ s
        : Js.String.slice(~from=0, ~to_=sl - t.decimals, s)
          ++ "."
          ++ Js.String.slice(~from=sl - t.decimals, ~to_=sl, s)
    )
    |> Common.removeRedundantZeros;
  };
};

let toStringPlain = (t: t): string => {
  Bigint.to_string(t.value);
};

/* For example token value 1.005 has 3 decimals, concert it to float 1.005  */
let toFloatWithDecimal = (t: t): float => Js.Float.fromString(toString(t));

/* For example token value 1.005 has 3 decimals, concert it to float 1005.0  */
let toFloatWithoutDecimal = (t: t): float => Bigint.to_float(toBigint(t));

let toStringWithCommas = (t: t): string => Common.addCommas(toString(t));

let toStringWithCommasTruncated = (decimals: int, t: t): string =>
  t |> toFloatWithDecimal |> Format.truncateDecimals(decimals);
