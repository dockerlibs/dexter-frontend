type t =
  | PointOne /* 0.1 */
  | Half /* 0.5 */
  | One /* 1.0 */
  | Custom(Valid.t(float));

let ofFloat = f => {
  switch (f) {
  | 0.1 => PointOne
  | 0.5 => Half
  | 1.0 => One
  | _ => Custom(Valid(f, Js.Float.toString(f)))
  };
};

let toFloat = f => {
  switch (f) {
  | PointOne => 0.1
  | Half => 0.5
  | One => 1.0
  | Custom(Valid(f, _)) => f
  | _ => 0.
  };
};

let toString = t =>
  switch (t) {
  | PointOne => "0.1%"
  | Half => "0.5%"
  | One => "1.0%"
  | Custom(Valid(f, _)) => Printf.sprintf("%.2f", f) ++ "%"
  | _ => ""
  };

let defaultSlippage = 0.5;
