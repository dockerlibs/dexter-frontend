open Dexter_TransactionUtils;

/*
 * transacion for xtzToToken, tokenToXtz and tokenToToken
 */
module Exchange = {
  type t = {
    destination: Tezos.Address.t,
    symbolIn: string,
    symbolOut: string,
    valueIn: InputType.t,
    valueOut: InputType.t,
  };

  let encode = (t: t): Js.Json.t => {
    Json.Encode.object_([
      ("destination", Tezos_Address.encode(t.destination)),
      ("symbolIn", Json.Encode.string(t.symbolIn)),
      ("symbolOut", Json.Encode.string(t.symbolOut)),
      ("valueIn", InputType.encode(t.valueIn)),
      ("valueOut", InputType.encode(t.valueOut)),
    ]);
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field(
          "destination",
          a => Tezos_Util.unwrapResult(Tezos_Address.decode(a)),
          json,
        ),
        field("symbolIn", string, json),
        field("symbolOut", string, json),
        field(
          "valueIn",
          a => Tezos_Util.unwrapResult(InputType.decode(a)),
          json,
        ),
        field(
          "valueOut",
          a => Tezos_Util.unwrapResult(InputType.decode(a)),
          json,
        ),
      )
    ) {
    | (destination, symbolIn, symbolOut, valueIn, valueOut) =>
      Belt.Result.Ok({destination, symbolIn, symbolOut, valueIn, valueOut})
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };

  // get the token receiver from the michelson body of a xtzToToken
  let getXtzToTokenToAddress =
      (parameters: Tezos_Operation_Alpha.Parameters.t)
      : option(Tezos_Address.t) => {
    switch (parameters.value) {
    | Tezos.(
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([Expression.StringExpression(toAddress), _]),
          _,
        )
      ) =>
      switch (Tezos.Address.ofString(toAddress)) {
      | Belt.Result.Ok(toAddress) => Some(toAddress)
      | _ => None
      }

    | _ => None
    };
  };

  // get the token receiver from the michelson body of a tokenToXtz
  let getTokenToXtzAddress =
      (parameters: Tezos_Operation_Alpha.Parameters.t)
      : option(Tezos_Address.t) => {
    switch (parameters.value) {
    | Tezos.(
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([_, Expression.StringExpression(toAddress)]),
              _,
            ),
            _,
          ]),
          _,
        )
      ) =>
      switch (Tezos.Address.ofString(toAddress)) {
      | Belt.Result.Ok(toAddress) => Some(toAddress)
      | _ => None
      }

    | _ => None
    };
  };

  // get the token value from the michelson body of a parameter of tokenToXtz
  let getTokenValue2 =
      (decimals: int, parameters: Tezos_Operation_Alpha.Parameters.t)
      : option(Tezos_Token.t) => {
    switch (parameters.value) {
    | Tezos.(
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            _,
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([Expression.IntExpression(tokenValue), _]),
              _,
            ),
          ]),
          _,
        )
      ) =>
      switch (Tezos.Token.ofBigint(tokenValue, decimals)) {
      | Belt.Result.Ok(token) => Some(token)
      | _ => None
      }

    | _ => None
    };
  };

  let ofOperationsToXtzToToken =
      (
        exchanges: list(Dexter_Exchange.t),
        xtzToTokenOperation: TZKT.Operation.t,
        operations: list(TZKT.Operation.t),
      )
      : option(t) => {
    let transferOperation = getNamedEntrypoint(operations, "transfer");

    switch (transferOperation) {
    | Some(transferOperation) =>
      let exchange =
        List.filter(
          (x: Dexter_Exchange.t) => {
            Tezos_Address.ofContract(x.dexterContract)
            == xtzToTokenOperation.target
          },
          exchanges,
        )
        |> Belt.List.head;

      switch (
        exchange,
        xtzToTokenOperation.parameters,
        transferOperation.parameters,
      ) {
      | (Some(exchange), Some(op1), Some(op2)) =>
        switch (
          getXtzToTokenToAddress(op1),
          getTokenValueFromParameters(exchange.decimals, op2),
        ) {
        | (Some(toAddress), Some(tokenOut)) =>
          Some({
            valueIn: InputType.Mutez(xtzToTokenOperation.amount),
            symbolIn: "XTZ",
            valueOut: InputType.Token(tokenOut),
            symbolOut: exchange.symbol,
            destination: toAddress,
          })
        | _ => None
        }

      | _ => None
      };

    | _ => None
    };
  };

  let ofOperationsToTokenToXtz =
      (
        exchanges: list(Dexter_Exchange.t),
        tokenToXtzOperation: TZKT.Operation.t,
        operations: list(TZKT.Operation.t),
      )
      : option(t) => {
    let transferOperation = getNamedEntrypoint(operations, "transfer");

    let xtzTransferOperation =
      List.filter(
        (x: TZKT.Operation.t) => {
          switch (x.parameters) {
          | None => true
          | Some(_) => false
          }
        },
        operations,
      )
      |> Belt.List.head;

    switch (transferOperation, xtzTransferOperation) {
    | (Some(_transferOperation), Some(xtzTransferOperation)) =>
      let exchange =
        List.filter(
          (x: Dexter_Exchange.t) => {
            Tezos_Address.ofContract(x.dexterContract)
            == tokenToXtzOperation.target
          },
          exchanges,
        )
        |> Belt.List.head;

      switch (exchange, tokenToXtzOperation.parameters) {
      | (Some(exchange), Some(op1)) =>
        switch (
          getTokenToXtzAddress(op1),
          getTokenValue2(exchange.decimals, op1),
        ) {
        | (Some(toAddress), Some(tokenOut)) =>
          Some({
            valueIn: InputType.Token(tokenOut),
            symbolIn: exchange.symbol,
            valueOut: InputType.Mutez(xtzTransferOperation.amount),
            symbolOut: "XTZ",
            destination: toAddress,
          })
        | _ => None
        }

      | _ => None
      };

    | _ => None
    };
  };
};

module AddLiquidity = {
  type t = {
    symbol: string,
    tokenIn: Tezos.Token.t,
    xtzIn: Tezos.Mutez.t,
  };

  let encode = (t: t): Js.Json.t => {
    Json.Encode.object_([
      ("symbol", Json.Encode.string(t.symbol)),
      ("tokenIn", Tezos.Token.encode(t.tokenIn)),
      ("xtzIn", Tezos.Mutez.encode(t.xtzIn)),
    ]);
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field("symbol", string, json),
        field(
          "tokenIn",
          a => Tezos_Util.unwrapResult(Tezos.Token.decode(a)),
          json,
        ),
        field(
          "xtzIn",
          a => Tezos_Util.unwrapResult(Tezos.Mutez.decode(a)),
          json,
        ),
      )
    ) {
    | (symbol, tokenIn, xtzIn) => Belt.Result.Ok({symbol, tokenIn, xtzIn})
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };

  let ofOperations =
      (
        exchanges: list(Dexter_Exchange.t),
        addLiquidityOperation: TZKT.Operation.t,
        operations: list(TZKT.Operation.t),
      )
      : option(t) => {
    let transferOperation = getNamedEntrypoint(operations, "transfer");

    switch (transferOperation) {
    | Some(transferOperation) =>
      let exchange =
        List.filter(
          (x: Dexter_Exchange.t) => {
            Tezos_Address.ofContract(x.dexterContract)
            == transferOperation.sender
          },
          exchanges,
        )
        |> Belt.List.head;
      switch (exchange, transferOperation.parameters) {
      | (Some(exchange), Some(op1)) =>
        switch (getTokenValueFromParameters(exchange.decimals, op1)) {
        | Some(tokenIn) =>
          Some({
            xtzIn: addLiquidityOperation.amount,
            tokenIn,
            symbol: exchange.symbol,
          })
        | _ => None
        }

      | _ => None
      };

    | _ => None
    };
  };
};

module RemoveLiquidity = {
  type t = {
    lqtBurned: Tezos.Token.t,
    symbol: string,
    tokenOut: Tezos.Token.t,
    xtzOut: Tezos.Mutez.t,
  };

  let encode = (t: t): Js.Json.t => {
    Json.Encode.object_([
      ("lqtBurned", Tezos.Token.encode(t.lqtBurned)),
      ("symbol", Json.Encode.string(t.symbol)),
      ("tokenOut", Tezos.Token.encode(t.tokenOut)),
      ("xtzOut", Tezos.Mutez.encode(t.xtzOut)),
    ]);
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field(
          "lqtBurned",
          a => Tezos_Util.unwrapResult(Tezos.Token.decode(a)),
          json,
        ),
        field("symbol", string, json),
        field(
          "tokenOut",
          a => Tezos_Util.unwrapResult(Tezos.Token.decode(a)),
          json,
        ),
        field(
          "xtzOut",
          a => Tezos_Util.unwrapResult(Tezos.Mutez.decode(a)),
          json,
        ),
      )
    ) {
    | (lqtBurned, symbol, tokenOut, xtzOut) =>
      Belt.Result.Ok({lqtBurned, symbol, tokenOut, xtzOut})
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };

  // get burned
  let getRemoveLiquidityLqtBurned =
      (parameters: Tezos_Operation_Alpha.Parameters.t): option(Tezos_Token.t) => {
    switch (parameters.value) {
    | Tezos.(
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                _,
                Expression.SingleExpression(
                  Primitives.PrimitiveData(PrimitiveData.Pair),
                  Some([_, Expression.IntExpression(lqtBurned)]),
                  _,
                ),
              ]),
              _,
            ),
            _,
          ]),
          _,
        )
      ) =>
      switch (Tezos.Token.ofBigint(lqtBurned, 0)) {
      | Belt.Result.Ok(lqtBurned) => Some(lqtBurned)
      | _ => None
      }

    | _ => None
    };
  };

  // get the token receiver from the michelson body of a xtzToToken
  let getRemoveLiquidityToAddress =
      (parameters: Tezos_Operation_Alpha.Parameters.t)
      : option(Tezos_Address.t) => {
    switch (parameters.value) {
    | Tezos.(
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                _,
                Expression.SingleExpression(
                  Primitives.PrimitiveData(PrimitiveData.Pair),
                  Some([Expression.StringExpression(toAddress), _]),
                  _,
                ),
              ]),
              _,
            ),
            _,
          ]),
          _,
        )
      ) =>
      switch (Tezos.Address.ofString(toAddress)) {
      | Belt.Result.Ok(toAddress) => Some(toAddress)
      | _ => None
      }

    | _ => None
    };
  };

  let ofOperations =
      (
        exchanges: list(Dexter_Exchange.t),
        removeLiquidityOperation: TZKT.Operation.t,
        operations: list(TZKT.Operation.t),
      )
      : option(t) => {
    let transferOperation = getNamedEntrypoint(operations, "transfer");

    let xtzTransferOperation =
      List.filter(
        (x: TZKT.Operation.t) => {
          switch (x.parameters) {
          | None => true
          | Some(_) => false
          }
        },
        operations,
      )
      |> Belt.List.head;
    switch (transferOperation, xtzTransferOperation) {
    | (Some(transferOperation), Some(xtzTransferOperation)) =>
      let exchange =
        List.filter(
          (x: Dexter_Exchange.t) => {
            Tezos_Address.ofContract(x.dexterContract)
            == transferOperation.sender
          },
          exchanges,
        )
        |> Belt.List.head;
      switch (
        exchange,
        removeLiquidityOperation.parameters,
        transferOperation.parameters,
      ) {
      | (Some(exchange), Some(op1), Some(op2)) =>
        switch (
          getRemoveLiquidityLqtBurned(op1),
          getTokenValueFromParameters(exchange.decimals, op2),
        ) {
        | (Some(lqtBurned), Some(tokenOut)) =>
          Some({
            lqtBurned,
            xtzOut: xtzTransferOperation.amount,
            tokenOut,
            symbol: exchange.symbol,
          })
        | _ => None
        }

      | _ => None
      };

    | _ => None
    };
  };
};

type t =
  | Exchange(Exchange.t)
  | AddLiquidity(AddLiquidity.t)
  | RemoveLiquidity(RemoveLiquidity.t);

let encode = (t: t): Js.Json.t => {
  Json.Encode.(
    switch (t) {
    | Exchange(exchange) =>
      object_([("exchange", Exchange.encode(exchange))])
    | AddLiquidity(addLiquidity) =>
      object_([("add_liquidity", AddLiquidity.encode(addLiquidity))])
    | RemoveLiquidity(removeLiquidity) =>
      object_([
        ("remove_liquidity", RemoveLiquidity.encode(removeLiquidity)),
      ])
    }
  );
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Js_json.decodeObject(json)) {
  | Some(obj) =>
    switch (Js_dict.keys(obj)) {
    | [|key|] =>
      switch (key) {
      | "exchange" =>
        switch (Exchange.decode(Js_dict.unsafeGet(obj, key))) {
        | Belt.Result.Ok(e) => Belt.Result.Ok(Exchange(e))
        | Belt.Result.Error(error) => Belt.Result.Error(error)
        }
      | "add_liquidity" =>
        switch (AddLiquidity.decode(Js_dict.unsafeGet(obj, key))) {
        | Belt.Result.Ok(e) => Belt.Result.Ok(AddLiquidity(e))
        | Belt.Result.Error(error) => Belt.Result.Error(error)
        }

      | "remove_liquidity" =>
        switch (RemoveLiquidity.decode(Js_dict.unsafeGet(obj, key))) {
        | Belt.Result.Ok(e) => Belt.Result.Ok(RemoveLiquidity(e))
        | Belt.Result.Error(error) => Belt.Result.Error(error)
        }
      | _ => Belt.Result.Error("decode: Invalid key")
      }

    | _ => Belt.Result.Error("decode: Expected only one key in JSON Object")
    }
  | _ => Belt.Result.Error("decode: Expected JSON Object")
  };
};

let updateValueAfterOperation =
    (
      t: t,
      contractId: string,
      metadata: Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.t,
    ) => {
  switch (t) {
  | Exchange(e) =>
    switch ((e.valueIn: InputType.t)) {
    | Mutez(_) =>
      switch ((e.valueOut: InputType.t)) {
      | Token(token) =>
        /* xtzToToken: update the output token value */
        switch (getTokenValue(token.decimals, metadata)) {
        | Some(tokenValue) =>
          Exchange({...e, valueOut: InputType.Token(tokenValue)})
        | None => t
        }
      | _ => t
      }
    | Token(_) =>
      switch ((e.valueOut: InputType.t)) {
      | Mutez(_) =>
        switch (getXtzValue(contractId, e.destination, metadata)) {
        | Some(xtzValue) =>
          Exchange({...e, valueOut: InputType.Mutez(xtzValue)})
        | None => t
        }

      | _ => t
      }
    }
  | AddLiquidity(_) => t
  | RemoveLiquidity(_) => t
  };
};
