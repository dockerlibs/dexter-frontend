let truncateDecimals =
    (~sign=true, ~round=true, decimals: int, value: float): string => {
  let minValue = Common.getLowestValueWithDecimals(decimals);

  switch (value) {
  | 0.0 => round ? "0" : Js.Float.toFixedWithPrecision(0.0, ~digits=decimals)
  | _ when value < (minValue |> float_of_string) =>
    (sign ? "<" : "") ++ minValue
  | _ =>
    let value = Js.Float.toFixedWithPrecision(value, ~digits=decimals);
    round ? value |> Js.Float.fromString |> Js.Float.toString : value;
  };
};

let truncateDecimalsFloat = (decimals: int, floatString: string): string => {
  let split = Js.String.split(".", floatString);

  Array.length(split) === 2 && String.length(split[1]) > decimals
    ? split[0] ++ "." ++ Js.String.slice(~from=0, ~to_=decimals, split[1])
    : floatString;
};
