let sequence =
    (results: list(Belt.Result.t('a, 'b))): Belt.Result.t(list('a), 'b) =>
  List.fold_left(
    (acc, result) =>
      switch (acc) {
      | Belt.Result.Ok(accs) =>
        switch (result) {
        | Belt.Result.Ok(ok) => Belt.Result.Ok(List.append(accs, [ok]))
        | Belt.Result.Error(err) => Belt.Result.Error(err)
        }

      | Belt.Result.Error(err) => Belt.Result.Error(err)
      },
    Belt.Result.Ok([]),
    results,
  );

/** With an list of options, eliminate all of the Nones, only keep the Somes */
let catSomes = (type a, mas: list(option(a))): list(a) =>
  List.fold_left(
    (acc, x) =>
      switch (x) {
      | None => acc
      | Some(a) => List.append(acc, [a])
      },
    [],
    mas,
  );

let getError = (r: Belt.Result.t('a, string)): string => {
  switch (r) {
  | Belt.Result.Error(err) => err
  | _ => ""
  };
};

let getErrors = (rs: list(Belt.Result.t('a, string))): string => {
  let oMsg =
    List.fold_left(
      (errors, result) => {
        switch (result) {
        | Belt.Result.Error(error) =>
          switch (errors) {
          | Some(errors) => Some(errors ++ ", " ++ error)
          | None => Some(error)
          }
        | _ => errors
        }
      },
      None,
      rs,
    );

  switch (oMsg) {
  | Some(msg) => msg
  | None => "No errors"
  };
};

let nbsp = [%raw {|'\u00a0'|}];
let iInCircle = [%raw {|'\u24D8'|}];
let alert = [%raw {|'\u26A0'|}];
let times = [%raw {|'\u2A09'|}];

/* add the appropriate amount of commas to a number string */
let addCommas: string => string = [%bs.raw
  {|
    function (s) {
      var sp = s.split(".");
      var l = sp[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      if (sp.length > 1) {
        return l.concat(".", sp[1]);
      } else {
        return l;
      }
    }
     |}
];

let removeCommas: string => string = [%bs.raw
  {|
    function (s) {
      return s.replace(/,/g, "");
    }
   |}
];

let getLowestValueWithDecimals = (decimals: int): string => {
  decimals > 0
    ? "0." ++ ("0" |> Tezos_Util.repeatString(decimals - 1)) ++ "1" : "1";
};

let getLowestValueWithDecimalsFloat = (decimals: int): float => {
  (
    decimals > 0
      ? "0." ++ ("0" |> Tezos_Util.repeatString(decimals - 1)) ++ "1" : "1"
  )
  |> Js.Float.fromString;
};

let getDecimalsCount = (floatString: string): int => {
  let split = Js.String.split(".", floatString);
  Array.length(split) == 2 ? String.length(split[1]) : 0;
};

let removeRedundantZeros = (floatString: string) => {
  floatString |> Js.String.includes(".")
    ? floatString
      |> Js.String.replaceByRe(Js.Re.fromString("0*$"), "")
      |> Js.String.replaceByRe(Js.Re.fromString("\.$"), "")
    : floatString;
};

let isSubstring = (string: string, substring: string) => {
  let string = Bytes.of_string(string);
  let substring = Bytes.of_string(substring);
  let ssl = Bytes.length(substring);
  let sl = Bytes.length(string);
  if (ssl == 0 || ssl > sl) {
    false;
  } else {
    let max = sl - ssl;
    let clone = Bytes.create(ssl);
    let rec check = pos =>
      pos <= max
      && {
        Bytes.blit(string, pos, clone, 0, ssl);
        clone == substring
        || check(
             Bytes.index_from(string, succ(pos), Bytes.get(substring, 0)),
           );
      };

    try(check(Bytes.index(string, Bytes.get(substring, 0)))) {
    | Not_found => false
    };
  };
};

[@bs.module] external uuid: unit => string = "uuid-random";

let promiseErrorToString: Js.Promise.error => string = [%bs.raw
  {|
   function (input) {
     return String(input);
   }
|}
];

let paste: unit => Js.Promise.t(string) = [%bs.raw
  {|
   function () {
     return navigator.clipboard.readText();
   }
|}
];

let reload: unit => unit = [%bs.raw
  {|
   function () {
     return location.reload();
   }
|}
];

let eventToValue = event => ReactEvent.Form.target(event)##value;

let packageVersion: unit => string = [%bs.raw
  {|
   function () {
     return VERSION;
   }
|}
];

let packageVersionTruncated = (): string =>
  packageVersion()
  |> Js.String.splitAtMost(".", ~limit=2)
  |> Js.Array.joinWith(".");

let isMaintenanceMode: unit => bool = [%bs.raw
  {|
     function () {
       return MAINTENANCE_MODE === "true";
     }
    |}
];

let isMobile: unit => bool = [%bs.raw
  {|
    function () {
      const a = navigator.userAgent || navigator.vendor || window.opera;

      return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i
        .test(a)
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
        .test(a.substr(0, 4))
    }
  |}
];

exception DecodeError(string);

let unwrapResult = r =>
  switch (r) {
  | Belt.Result.Ok(v) => v
  | Belt.Result.Error(message) => raise @@ DecodeError(message)
  };

// unsafe unwrap, useful for testing
let unwrapOption = r =>
  switch (r) {
  | Some(v) => v
  | None => raise @@ DecodeError("None")
  };

exception FetchError(string);

let rec fetchWithRetries = (~fetchFn, ~retries, ~identifier, ~service, ()) =>
  fetchFn()
  |> Js.Promise.catch(_error => {
       retries > 0
         ? fetchWithRetries(
             ~fetchFn,
             ~identifier,
             ~retries=retries - 1,
             ~service,
             (),
           )
         : {
           DexterUiContext_Services.emitEvent(~service, ());

           Js.Promise.make((~resolve, ~reject as _) => {
             Js.Global.setTimeout(
               () =>
                 fetchWithRetries(
                   ~fetchFn,
                   ~retries=0,
                   ~identifier,
                   ~service,
                   (),
                 )
                 |> Js.Promise.then_(result => {
                      DexterUiContext_Services.emitEvent(
                        ~service,
                        ~isUnavailable=false,
                        (),
                      );
                      resolve(. result) |> Js.Promise.resolve;
                    })
                 |> ignore,
               10000,
             )
             |> ignore
           });
         }
     });

let fetch =
    (
      ~decode,
      ~identifier: string,
      ~requestInit: Fetch.requestInit,
      ~retries: int=5,
      ~service: Service.t,
      ~url: string,
      (),
    )
    : Js.Promise.t(Belt.Result.t('a, string)) =>
  fetchWithRetries(
    ~fetchFn=
      () =>
        Js.Promise.(
          Fetch.fetchWithInit(url, requestInit)
          |> then_(Fetch.Response.json)
          |> then_(json =>
               switch (json |> decode |> unwrapResult) {
               | data => Belt.Result.Ok(data) |> resolve
               }
             )
        ),
    ~identifier,
    ~retries,
    ~service,
    (),
  );

let fetchOpt =
    (
      ~decode,
      ~identifier: string,
      ~requestInit: Fetch.requestInit,
      ~retries: int=5,
      ~service: Service.t,
      ~url: string,
      (),
    )
    : Js.Promise.t(Belt.Result.t('a, string)) =>
  fetchWithRetries(
    ~fetchFn=
      () =>
        Js.Promise.(
          Fetch.fetchWithInit(url, requestInit)
          |> then_(response => {
               let status = Fetch.Response.status(response);

               if (status >= 200 && status < 300) {
                 Fetch.Response.json(response)
                 |> then_(json =>
                      switch (json |> decode |> unwrapResult) {
                      | data => Belt.Result.Ok(data) |> resolve
                      }
                    );
               } else if (status == 404) {
                 Belt.Result.Ok(None) |> resolve;
               } else {
                 FetchError("Response status: " ++ (status |> string_of_int))
                 |> reject;
               };
             })
        ),
    ~identifier,
    ~retries,
    ~service,
    (),
  );
