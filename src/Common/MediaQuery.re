[@bs.module "react-responsive"]
external mediaQuery: ReasonReact.reactClass = "default";

let make = (~query: string, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass=mediaQuery,
    ~props={"query": query},
    children,
  );

module Compat = {
  [@bs.module "react-responsive"] [@react.component]
  external make: (~query: string, ~children: React.element) => React.element =
    "default";
};
