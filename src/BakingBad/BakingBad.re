// https://api.better-call.dev/v1/account/mainnet?address=tz1fSkEwBCgTLas8Y82SYpEGW9aFZPBag8uY

module TokenBalance = {
  type t = {
    contract: string,
    balance: Bigint.t,
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
    switch (
      Json.Decode.field("contract", Json.Decode.string, json),
      Json.Decode.field(
        "balance",
        a => Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a)),
        json,
      ),
    ) {
    | (contract, balance) => Belt.Result.Ok({contract, balance})
    | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
    };
};

let toMap = (ts: list(TokenBalance.t)): Belt.Map.String.t(Bigint.t) => {
  List.map((t: TokenBalance.t) => (t.contract, t.balance), ts)
  |> Array.of_list
  |> Belt.Map.String.fromArray;
};

let decodeForAddress =
    (address: string, json: Js.Json.t)
    : Belt.Result.t(Belt.Map.String.t(Bigint.t), string) => {
  switch (
    Json.Decode.field(
      address,
      internal_ => {
        Json.Decode.list(
          a => Tezos_Util.unwrapResult(TokenBalance.decode(a)),
          internal_,
        )
      },
      json,
    )
  ) {
    | currenciesState => Belt.Result.Ok(currenciesState |> toMap)
  | exception (Json.Decode.DecodeError(_err)) => Belt.Result.Ok(Belt.Map.String.empty)
  /* | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err) */
  };
};

let getTokenBalances =
    (address: Tezos.Address.t)
    : Js.Promise.t(Belt.Result.t(Belt.Map.String.t(Bigint.t), string)) =>
  Common.fetch(
    ~decode=decodeForAddress(Tezos.Address.toString(address)),
    ~identifier="BakingBad.getTokenBalances",
    ~url=
      "https://api.better-call.dev/v1/account/"
      ++ Dexter_Settings.bakingBadNetwork
      ++ "?address="
      ++ Tezos.Address.toString(address),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.BakingBad,
    (),
  );
