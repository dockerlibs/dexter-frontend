# Automated Market Maker

A market maker is a platform that provides a buy price and a sell price for a 
commodity that it buys and sells. Generally this service will be provided
continuously. The buy and sell prices are decided by the provider.

An automated market maker (AMM) is a similar platform but it uses code or logic
to automatically determine the buy and sell prices and to guarantee that 
commodities exists to trade.

Dexter is an automated market maker for FA1.2 tokens on the Tezos blockchain.
The AMM logic is powered by a smart contract on the Tezos blockchain. For each
FA1.2 token we want to support in a Dexter exchange, there needs to be a Dexter
smart contract paired with it.

The buy and sell values of XTZ and FA1.2 held by a Dexter exchange is determined
by the balance of each value. There is also a penalty for large trades that 
prevent either balance from becoming zero or from traders reducing the balances 
to small amounts. As long as there is at least one liquidity provider and the 
balance of XTZ is greater than 0.000001 and the balance of FA1.2 is greater than 1, 
there will be an automated market.
